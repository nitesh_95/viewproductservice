package com.bhushan.spring.files.excel.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bhushan.spring.files.excel.model.ServiceFeedbackForm;

@Repository
public interface ServiceFeedbackFormRepo extends JpaRepository<ServiceFeedbackForm, Integer>{

	@Query(value = "select * from servicefeedbackform where customerid=?1",nativeQuery = true)
	ServiceFeedbackForm findbycustomerid(@Param(value = "customerid") int customerid);

}
