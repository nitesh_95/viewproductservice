package com.bhushan.spring.files.excel.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bhushan.spring.files.excel.model.ServicingEntity;

@Repository
public interface ServicingdetailsRepo extends JpaRepository<ServicingEntity, Integer> {

	@Query(value = "select * from servicingdetails where ticketno=?1 and mobileno=?2", nativeQuery = true)
	Optional<ServicingEntity>  findbyticketnoandmobileno(@Param(value = "ticketno") String ticketno,
			@Param(value = "mobileno") Long mobileno);
}
