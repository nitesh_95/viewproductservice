package com.bhushan.spring.files.excel.response;

import com.bhushan.spring.files.excel.model.OfflineCustomerDetails;
import com.bhushan.spring.files.excel.model.OnlineCustomerRequest;

public class OnlineCustomerResponse {
	private String status;
	private String message;
	private OnlineCustomerRequest productpurchaseddetails;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public OnlineCustomerRequest getProductpurchaseddetails() {
		return productpurchaseddetails;
	}

	public void setProductpurchaseddetails(OnlineCustomerRequest productpurchaseddetails) {
		this.productpurchaseddetails = productpurchaseddetails;
	}

}
