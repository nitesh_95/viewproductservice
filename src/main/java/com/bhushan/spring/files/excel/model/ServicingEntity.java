package com.bhushan.spring.files.excel.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "servicingdetails")
public class ServicingEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int customerids;
	private String customertype;
	private String name;
	private String mailid;
	private String address;
	private Long mobileno;
	private String model;
	private String type;
	private String sparepart;
	private String productname;
	private String dateofpurchase;
	private String dateofservicerequest;
	private String assignedto;
	private boolean iswarranty;
	private double charge;
	private double techniciancost;
	private float tax;
	private double totalcost;
	private double paidamount;
	private String ticketno;
	private String dateofresolvingissue;
	private String servicingdate;
	private double remainingamount;
	private String ticketstatus;
	private String invoiceno;
	private String otp;
	private double paidservicingamount;
	@Column(name = "issuedescription", nullable = true,  length = 500)
	private String issuedescription;
	
	
	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public String getAssignedto() {
		return assignedto;
	}

	public void setAssignedto(String assignedto) {
		this.assignedto = assignedto;
	}

	public String getIssuedescription() {
		return issuedescription;
	}

	public void setIssuedescription(String issuedescription) {
		this.issuedescription = issuedescription;
	}

	public double getPaidservicingamount() {
		return paidservicingamount;
	}

	public void setPaidservicingamount(double paidservicingamount) {
		this.paidservicingamount = paidservicingamount;
	}

	public String getInvoiceno() {
		return invoiceno;
	}

	public void setInvoiceno(String invoiceno) {
		this.invoiceno = invoiceno;
	}

	public String getTicketstatus() {
		return ticketstatus;
	}

	public void setTicketstatus(String ticketstatus) {
		this.ticketstatus = ticketstatus;
	}

	public double getRemainingamount() {
		return remainingamount;
	}

	public void setRemainingamount(double remainingamount) {
		this.remainingamount = remainingamount;
	}

	public String getCustomertype() {
		return customertype;
	}

	public void setCustomertype(String customertype) {
		this.customertype = customertype;
	}

	public String getTicketno() {
		return ticketno;
	}

	public void setTicketno(String ticketno) {
		this.ticketno = ticketno;
	}

	public String getDateofresolvingissue() {
		return dateofresolvingissue;
	}

	public void setDateofresolvingissue(String dateofresolvingissue) {
		this.dateofresolvingissue = dateofresolvingissue;
	}

	public String getServicingdate() {
		return servicingdate;
	}

	public void setServicingdate(String servicingdate) {
		this.servicingdate = servicingdate;
	}

	public String getProductname() {
		return productname;
	}

	public void setProductname(String productname) {
		this.productname = productname;
	}

	public String getDateofpurchase() {
		return dateofpurchase;
	}

	public void setDateofpurchase(String dateofpurchase) {
		this.dateofpurchase = dateofpurchase;
	}

	public String getDateofservicerequest() {
		return dateofservicerequest;
	}

	public void setDateofservicerequest(String dateofservicerequest) {
		this.dateofservicerequest = dateofservicerequest;
	}

	public boolean isIswarranty() {
		return iswarranty;
	}

	public void setIswarranty(boolean iswarranty) {
		this.iswarranty = iswarranty;
	}

	public double getTechniciancost() {
		return techniciancost;
	}

	public void setTechniciancost(double techniciancost) {
		this.techniciancost = techniciancost;
	}

	public float getTax() {
		return tax;
	}

	public void setTax(float tax) {
		this.tax = tax;
	}

	public double getTotalcost() {
		return totalcost;
	}

	public void setTotalcost(double totalcost) {
		this.totalcost = totalcost;
	}

	public double getPaidamount() {
		return paidamount;
	}

	public void setPaidamount(double paidamount) {
		this.paidamount = paidamount;
	}

	public int getCustomerids() {
		return customerids;
	}

	public void setCustomerids(int customerids) {
		this.customerids = customerids;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMailid() {
		return mailid;
	}

	public void setMailid(String mailid) {
		this.mailid = mailid;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Long getMobileno() {
		return mobileno;
	}

	public void setMobileno(Long mobileno) {
		this.mobileno = mobileno;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public double getCharge() {
		return charge;
	}

	public void setCharge(double charge) {
		this.charge = charge;
	}

	public String getSparepart() {
		return sparepart;
	}

	public void setSparepart(String sparepart) {
		this.sparepart = sparepart;
	}

}
