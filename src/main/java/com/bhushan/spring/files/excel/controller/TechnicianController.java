package com.bhushan.spring.files.excel.controller;

import java.io.IOException;
import java.util.List;

import org.apache.http.client.ClientProtocolException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bhushan.spring.files.excel.model.TechnicianPortal;
import com.bhushan.spring.files.excel.response.TechnicianPortalResponse;
import com.bhushan.spring.files.excel.response.TechnicianResponse;
import com.bhushan.spring.files.excel.service.TechnicianService;

@RestController
@CrossOrigin
@RequestMapping("/api/excel")
public class TechnicianController {
	@Autowired
	private TechnicianService technicianService;

	@PostMapping("/saveTechnicianDetails")
	public TechnicianPortalResponse saveData(@RequestBody TechnicianPortal technicianPortal)
			throws ClientProtocolException, IOException {
		TechnicianPortalResponse saveData = technicianService.saveData(technicianPortal);
		return saveData;
	}

	@GetMapping("/technicianlist")
	public List<TechnicianPortal> findallstock() {
		return technicianService.findall();
	}

	@PostMapping("/technician/gettotalamount")
	public double gettotalamount(@RequestBody TechnicianPortal technicianPortal) {
		return technicianService.getTotalAmount(technicianPortal);
	}

	@GetMapping("/technicianservice/{id}")
	public TechnicianPortal deleteData(@PathVariable int id) throws Exception {
		return technicianService.findbyid(id);
	}

	@GetMapping("/technicianportalcount/{assignedto}")
	public TechnicianResponse technicianPortalCount(@PathVariable String assignedto) {
		return technicianService.technicianPortalCount(assignedto);
	}

	
}
