package com.bhushan.spring.files.excel.service;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.stereotype.Service;

import com.bhushan.spring.files.excel.model.EmployeeEntity;
import com.bhushan.spring.files.excel.model.OfflineCustomerDetails;
import com.bhushan.spring.files.excel.model.OnlineCustomerRequest;
import com.bhushan.spring.files.excel.model.ServicingEntity;
import com.bhushan.spring.files.excel.model.TechnicianPortal;

@Service
public class MessageUtil {
	public static HttpResponse sendMsgToEmployee(EmployeeEntity employeeEntity)
			throws ClientProtocolException, IOException {

		String authKey = "3xse0JRjBfp7l2hKm6WYTMAbocHOkFQSnNvwGg1E8UPiVrZ9yCTX6OFPbCaNVqWphIzL1i8MlrSngAjo";
		String msg = "Hi " + " " + employeeEntity.getName() + " "
				+ "You have been successfully registered with DelightR0 you can login with your userName " + " "
				+ employeeEntity.getAdharno() + " " + "and Password is " + " " + employeeEntity.getPassword() + " "
				+ "you can login in with your credentials on" + " " + "https://bit.ly/3i2J36O";
		HttpClient client = HttpClientBuilder.create().build();
		String url = "https://www.fast2sms.com/dev/bulk?authorization=" + authKey + "&sender_id=FSTSMS&message=" + msg
				+ "&language=english&route=p&numbers=" + employeeEntity.getMobileno();
		String urls = url.replaceAll(" ", "%20");
		HttpUriRequest httpUriRequest = new HttpGet(urls);
		HttpResponse response = client.execute(httpUriRequest);
		return response;
	}

	public static HttpResponse sendTechnicianMsg(ServicingEntity servicingEntity, Long mobileno)
			throws ClientProtocolException, IOException {
		String authKey = "3xse0JRjBfp7l2hKm6WYTMAbocHOkFQSnNvwGg1E8UPiVrZ9yCTX6OFPbCaNVqWphIzL1i8MlrSngAjo";
		String msg = "Hi " + " " + servicingEntity.getAssignedto() + " !!"
				+ "You have recieved a New Ticket with Ticket no " + " " + servicingEntity.getTicketno() + " "
				+ "customer contact no is as follows" + " " + servicingEntity.getMobileno() + " "
				+ "For more details login to" + " " + "https://bit.ly/3i2J36O" + " "
				+ "Thanks for being the part of, DelightROPoint";
		HttpClient client = HttpClientBuilder.create().build();
		String url = "https://www.fast2sms.com/dev/bulk?authorization=" + authKey + "&sender_id=FSTSMS&message=" + msg
				+ "&language=english&route=p&numbers=" + mobileno;
		String urls = url.replaceAll(" ", "%20");
		HttpUriRequest httpUriRequest = new HttpGet(urls);
		HttpResponse response = client.execute(httpUriRequest);
		return response;
	}

	public static HttpResponse sendonlineservicingmessage(String name, Long mobileno)
			throws ClientProtocolException, IOException {
		String authKey = "3xse0JRjBfp7l2hKm6WYTMAbocHOkFQSnNvwGg1E8UPiVrZ9yCTX6OFPbCaNVqWphIzL1i8MlrSngAjo";
		String msg = "Hi " + " " + name + " " + "Your service request has been taken" + " "
				+ "we will update you once it gets confirmed by our team " + "Thanks for Trusting us, DelightROPoint";
		HttpClient client = HttpClientBuilder.create().build();
		String url = "https://www.fast2sms.com/dev/bulk?authorization=" + authKey + "&sender_id=FSTSMS&message=" + msg
				+ "&language=english&route=p&numbers=" + mobileno;
		String urls = url.replaceAll(" ", "%20");
		HttpUriRequest httpUriRequest = new HttpGet(urls);
		HttpResponse response = client.execute(httpUriRequest);
		return response;
	}

	public static HttpResponse sendServicingMessage(ServicingEntity servicingEntity)
			throws ClientProtocolException, IOException {
		String authKey = "3xse0JRjBfp7l2hKm6WYTMAbocHOkFQSnNvwGg1E8UPiVrZ9yCTX6OFPbCaNVqWphIzL1i8MlrSngAjo";
		String msg = "Hi " + " " + servicingEntity.getName() + " "
				+ "Your service request has been accepted and its assigned to " + servicingEntity.getAssignedto() + " "
				+ "and the issue will be fixed by " + " " + servicingEntity.getDateofresolvingissue() + " "
				+ " share the OTP once you are satisfied with our servicing" + " " + servicingEntity.getOtp() + " "
				+ "Thanks for Trusting us, DelightROPoint";
		HttpClient client = HttpClientBuilder.create().build();
		String url = "https://www.fast2sms.com/dev/bulk?authorization=" + authKey + "&sender_id=FSTSMS&message=" + msg
				+ "&language=english&route=p&numbers=" + servicingEntity.getMobileno();
		String urls = url.replaceAll(" ", "%20");
		HttpUriRequest httpUriRequest = new HttpGet(urls);
		HttpResponse response = client.execute(httpUriRequest);
		return response;
	}

	public static HttpResponse sendOnlineCustomerDetails(OnlineCustomerRequest productPurchasedSaveDetails)
			throws ClientProtocolException, IOException {
		String authKey = "3xse0JRjBfp7l2hKm6WYTMAbocHOkFQSnNvwGg1E8UPiVrZ9yCTX6OFPbCaNVqWphIzL1i8MlrSngAjo";
		String msg = "Hi " + " " + productPurchasedSaveDetails.getName() + " "
				+ "You order has been successfully placed  with OrderId" + " " + productPurchasedSaveDetails.getId()
				+ " " + "Your Order will be delivered within 7 days" + " " + "Login to know more" + " "
				+ "https://bit.ly/39LsLLn" + " " + " " + "DelightROPoint";
		HttpClient client = HttpClientBuilder.create().build();
		String url = "https://www.fast2sms.com/dev/bulk?authorization=" + authKey + "&sender_id=FSTSMS&message=" + msg
				+ "&language=english&route=p&numbers=" + productPurchasedSaveDetails.getMobileno()

		;
		String urls = url.replaceAll(" ", "%20");
		HttpUriRequest httpUriRequest = new HttpGet(urls);
		HttpResponse response = client.execute(httpUriRequest);
		System.out.println(response);
		return response;
	}

	public static HttpResponse sendOnlineCustomerDetailstoadmin(OnlineCustomerRequest productPurchasedSaveDetails)
			throws ClientProtocolException, IOException {
		String authKey = "3xse0JRjBfp7l2hKm6WYTMAbocHOkFQSnNvwGg1E8UPiVrZ9yCTX6OFPbCaNVqWphIzL1i8MlrSngAjo";
		String msg = "Hi Manoj ji" + " " + "You have recieved an online order of Rs"
				+ productPurchasedSaveDetails.getPurchasedetails().getTotalamount() + " " + "having OrderId" + " "
				+ productPurchasedSaveDetails.getId() + " " + "check your portal for more details" + " "
				+ "https://bit.ly/2XRZE6C" + " " + "DelightROPoint";
		HttpClient client = HttpClientBuilder.create().build();
		String url = "https://www.fast2sms.com/dev/bulk?authorization=" + authKey + "&sender_id=FSTSMS&message=" + msg
				+ "&language=english&route=p&numbers=" + productPurchasedSaveDetails.getMobileno();
		String urls = url.replaceAll(" ", "%20");
		HttpUriRequest httpUriRequest = new HttpGet(urls);
		HttpResponse response = client.execute(httpUriRequest);
		System.out.println(response);
		return response;
	}

	public static HttpResponse orderCancellation(OnlineCustomerRequest productPurchasedSaveDetails)
			throws ClientProtocolException, IOException {
		String authKey = "3xse0JRjBfp7l2hKm6WYTMAbocHOkFQSnNvwGg1E8UPiVrZ9yCTX6OFPbCaNVqWphIzL1i8MlrSngAjo";
		String msg = "Hi " + " " + productPurchasedSaveDetails.getName() + " "
				+ "You order has been successfully Cancelled with OrderId" + " " + productPurchasedSaveDetails.getId()
				+ " " + "Please Provide us suitable feedback to improve our services" + " "
				+ "Thanks and Regards , DelightROPoint";
		HttpClient client = HttpClientBuilder.create().build();
		String url = "https://www.fast2sms.com/dev/bulk?authorization=" + authKey + "&sender_id=FSTSMS&message=" + msg
				+ "&language=english&route=p&numbers=" + productPurchasedSaveDetails.getMobileno();
		String urls = url.replaceAll(" ", "%20");
		HttpUriRequest httpUriRequest = new HttpGet(urls);
		HttpResponse response = client.execute(httpUriRequest);
		return response;
	}

	public static HttpResponse orderCancellationAdmin(OnlineCustomerRequest productPurchasedSaveDetails)
			throws ClientProtocolException, IOException {
		String authKey = "3xse0JRjBfp7l2hKm6WYTMAbocHOkFQSnNvwGg1E8UPiVrZ9yCTX6OFPbCaNVqWphIzL1i8MlrSngAjo";
		String msg = "Hi " + " " + "Manoj ji" + " " + "Customer with OrderId" + " "
				+ productPurchasedSaveDetails.getId() + " " + "has successfully cancelled the order" + " " + "Dated"
				+ productPurchasedSaveDetails.getCancelleddate();
		HttpClient client = HttpClientBuilder.create().build();
		String url = "https://www.fast2sms.com/dev/bulk?authorization=" + authKey + "&sender_id=FSTSMS&message=" + msg
				+ "&language=english&route=p&numbers=" + productPurchasedSaveDetails.getMobileno();
		String urls = url.replaceAll(" ", "%20");
		HttpUriRequest httpUriRequest = new HttpGet(urls);
		HttpResponse response = client.execute(httpUriRequest);
		return response;
	}

	public static HttpResponse onlineOrderValueUpdateByAdmin(OnlineCustomerRequest productPurchasedSaveDetails)
			throws ClientProtocolException, IOException {
		String authKey = "3xse0JRjBfp7l2hKm6WYTMAbocHOkFQSnNvwGg1E8UPiVrZ9yCTX6OFPbCaNVqWphIzL1i8MlrSngAjo";
		String msg = "Hi " + " " + productPurchasedSaveDetails.getName() + " " + "Your order with OrderId" + " "
				+ productPurchasedSaveDetails.getId() + " " + "has been updated please check the link" + " "
				+ "https://bit.ly/39LsLLn for more details" + " " + "your date of delivery is "
				+ productPurchasedSaveDetails.getPaymenthistory().getDateofdelivery() + "Thanks for Shopping with us"
				+ " " + "DelightROPoint";
		;
		HttpClient client = HttpClientBuilder.create().build();
		String url = "https://www.fast2sms.com/dev/bulk?authorization=" + authKey + "&sender_id=FSTSMS&message=" + msg
				+ "&language=english&route=p&numbers=" + productPurchasedSaveDetails.getMobileno();
		String urls = url.replaceAll(" ", "%20");
		HttpUriRequest httpUriRequest = new HttpGet(urls);
		HttpResponse response = client.execute(httpUriRequest);
		return response;
	}

	public static HttpResponse sendInfoForOnlineCustomer(OfflineCustomerDetails offlineCustomerDetails)
			throws ClientProtocolException, IOException {
		String msg = null;
		String authKey = "3xse0JRjBfp7l2hKm6WYTMAbocHOkFQSnNvwGg1E8UPiVrZ9yCTX6OFPbCaNVqWphIzL1i8MlrSngAjo";
		if (offlineCustomerDetails.getBillingdetails().getEmidata() != null) {
			msg = "Hi " + " " + offlineCustomerDetails.getName() + " " + "Your order with OrderId" + " "
					+ offlineCustomerDetails.getInvoiceno() + " " + "has been Purchased worth" + " "
					+ offlineCustomerDetails.getBillingdetails().getEntirerate() + " "
					+ "PaidAmount for this purchase was" + " "
					+ offlineCustomerDetails.getBillingdetails().getPaidamount() + "and emi for"
					+ offlineCustomerDetails.getBillingdetails().getEmidata().getNoofmonths() + "months" + " "
					+ "will be applied on the remaining amount of" + " "
					+ offlineCustomerDetails.getBillingdetails().getRemainingamount() + "at the rate of " + " "
					+ offlineCustomerDetails.getBillingdetails().getOtherTaxRate() + " " + "monthly emi will be of"
					+ offlineCustomerDetails.getBillingdetails().getEmidata().getMontlyinstallment()
					+ "Thanks for doing Shopping with us" + " " + "DelightROPoint";
			;
		} else {
			msg = "Hi " + " " + offlineCustomerDetails.getName() + " " + "Your order with OrderId" + " "
					+ offlineCustomerDetails.getInvoiceno() + " " + "has been Purchased worth" + " "
					+ offlineCustomerDetails.getBillingdetails().getEntirerate() + " "
					+ "PaidAmount for this purchase was" + " "
					+ offlineCustomerDetails.getBillingdetails().getPaidamount() + "Thanks for doing Shopping with us"
					+ " " + "DelightROPoint";
		}
		HttpClient client = HttpClientBuilder.create().build();
		String url = "https://www.fast2sms.com/dev/bulk?authorization=" + authKey + "&sender_id=FSTSMS&message=" + msg
				+ "&language=english&route=p&numbers=" + offlineCustomerDetails.getMobileno();
		String urls = url.replaceAll(" ", "%20");
		HttpUriRequest httpUriRequest = new HttpGet(urls);
		HttpResponse response = client.execute(httpUriRequest);
		System.out.println(response);
		return response;
	}

	public static HttpResponse servicingClientMessage(TechnicianPortal technicianportal)
			throws ClientProtocolException, IOException {
		String authKey = "3xse0JRjBfp7l2hKm6WYTMAbocHOkFQSnNvwGg1E8UPiVrZ9yCTX6OFPbCaNVqWphIzL1i8MlrSngAjo";
		String msg = "Hi " + " " + technicianportal.getName() + " " + "Your Servicing request with ticket no" + " "
				+ technicianportal.getTicketno() + " " + "has been completed by" + " "
				+ technicianportal.getAssignedto() + " " + "Thanks for having trust with us" + " " + "DelightROPoint";
		;
		HttpClient client = HttpClientBuilder.create().build();
		String url = "https://www.fast2sms.com/dev/bulk?authorization=" + authKey + "&sender_id=FSTSMS&message=" + msg
				+ "&language=english&route=p&numbers=" + technicianportal.getMobileno();
		String urls = url.replaceAll(" ", "%20");
		HttpUriRequest httpUriRequest = new HttpGet(urls);
		HttpResponse response = client.execute(httpUriRequest);
		return response;
	}

	public static HttpResponse sendmessagetotechnician(TechnicianPortal technicianportal)
			throws ClientProtocolException, IOException {
		String authKey = "3xse0JRjBfp7l2hKm6WYTMAbocHOkFQSnNvwGg1E8UPiVrZ9yCTX6OFPbCaNVqWphIzL1i8MlrSngAjo";
		String msg = "Hi " + " " + technicianportal.getAssignedto() + " " + "as confirmed by you the ticketno" + " "
				+ technicianportal.getTicketno() + " " + "has been closed " + " " + "DelightROPoint";
		;
		HttpClient client = HttpClientBuilder.create().build();
		String url = "https://www.fast2sms.com/dev/bulk?authorization=" + authKey + "&sender_id=FSTSMS&message=" + msg
				+ "&language=english&route=p&numbers=" + technicianportal.getMobileno();
		String urls = url.replaceAll(" ", "%20");
		HttpUriRequest httpUriRequest = new HttpGet(urls);
		HttpResponse response = client.execute(httpUriRequest);
		return response;

	}

	public static HttpResponse sendmsgtoadmin(TechnicianPortal technicianportal)
			throws ClientProtocolException, IOException {
		String authKey = "3xse0JRjBfp7l2hKm6WYTMAbocHOkFQSnNvwGg1E8UPiVrZ9yCTX6OFPbCaNVqWphIzL1i8MlrSngAjo";
		String msg = "Hi " + " " + "Manojji " + " " + "The Servicing request with ticket no" + " "
				+ technicianportal.getTicketno() + " " + "has been completed by" + " "
				+ technicianportal.getAssignedto() + " " + "dated" + technicianportal.getDateofresolvingissue() + " "
				+ "bill generated by him was" + technicianportal.getTotalcost()
				+ "you can check furhter more by logging in to the portal ";
		;
		HttpClient client = HttpClientBuilder.create().build();
		String url = "https://www.fast2sms.com/dev/bulk?authorization=" + authKey + "&sender_id=FSTSMS&message=" + msg
				+ "&language=english&route=p&numbers=" + technicianportal.getMobileno();
		String urls = url.replaceAll(" ", "%20");
		HttpUriRequest httpUriRequest = new HttpGet(urls);
		HttpResponse response = client.execute(httpUriRequest);
		return response;

	}

	public static HttpResponse resendOtp(ServicingEntity servicingEntity) throws ClientProtocolException, IOException {
		System.out.println("Message sent successfully");
		String authKey = "3xse0JRjBfp7l2hKm6WYTMAbocHOkFQSnNvwGg1E8UPiVrZ9yCTX6OFPbCaNVqWphIzL1i8MlrSngAjo";
		String msg = "Hi " + " " + servicingEntity.getName() + " " + "Your Servicing request has been completed by"
				+ " " + servicingEntity.getAssignedto() + " " + "please share the OTP " + " " + servicingEntity.getOtp()
				+ " " + "with the service man to confirm the service request" + " " + "DelightROPoint";
		;
		HttpClient client = HttpClientBuilder.create().build();
		String url = "https://www.fast2sms.com/dev/bulk?authorization=" + authKey + "&sender_id=FSTSMS&message=" + msg
				+ "&language=english&route=p&numbers=" + servicingEntity.getMobileno();
		String urls = url.replaceAll(" ", "%20");
		HttpUriRequest httpUriRequest = new HttpGet(urls);
		HttpResponse response = client.execute(httpUriRequest);
		return response;
	}

}
