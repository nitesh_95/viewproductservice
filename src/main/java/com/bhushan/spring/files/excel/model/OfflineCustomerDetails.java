package com.bhushan.spring.files.excel.model;

import java.util.HashMap;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "offlinecustomerdetails")
public class OfflineCustomerDetails {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int offlinecustomerid;
	private String name;
	private String mailid;
	private long mobileno;
	private String dateofpurchase;
	private String invoiceno;
	private String address;
	private HashMap<String, HashMap<Double, Double>> getdetails;
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "offlinecustomerid")
	private List<ProductPurchased> productPurchaseds;
	@ManyToOne(cascade = { CascadeType.ALL })
	private Billingdetails billingdetails;

	public HashMap<String, HashMap<Double, Double>> getGetdetails() {
		return getdetails;
	}

	public void setGetdetails(HashMap<String, HashMap<Double, Double>> getdetails) {
		this.getdetails = getdetails;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getOfflinecustomerid() {
		return offlinecustomerid;
	}

	public String getDateofpurchase() {
		return dateofpurchase;
	}

	public void setDateofpurchase(String dateofpurchase) {
		this.dateofpurchase = dateofpurchase;
	}

	public void setOfflinecustomerid(int offlinecustomerid) {
		this.offlinecustomerid = offlinecustomerid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMailid() {
		return mailid;
	}

	public void setMailid(String mailid) {
		this.mailid = mailid;
	}

	public long getMobileno() {
		return mobileno;
	}

	public void setMobileno(long mobileno) {
		this.mobileno = mobileno;
	}

	public String getInvoiceno() {
		return invoiceno;
	}

	public void setInvoiceno(String invoiceno) {
		this.invoiceno = invoiceno;
	}

	public List<ProductPurchased> getProductPurchaseds() {
		return productPurchaseds;
	}

	public void setProductPurchaseds(List<ProductPurchased> productPurchaseds) {
		this.productPurchaseds = productPurchaseds;
	}

	public Billingdetails getBillingdetails() {
		return billingdetails;
	}

	public void setBillingdetails(Billingdetails billingdetails) {
		this.billingdetails = billingdetails;
	}

}
