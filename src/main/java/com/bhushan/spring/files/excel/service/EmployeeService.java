package com.bhushan.spring.files.excel.service;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.http.client.ClientProtocolException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bhushan.spring.files.excel.model.EmployeeEntity;
import com.bhushan.spring.files.excel.model.LoginEntity;
import com.bhushan.spring.files.excel.repository.OnlineCustomerRequestRepo;
import com.bhushan.spring.files.excel.repository.EmployeeDetailsRepo;
import com.bhushan.spring.files.excel.repository.FeedbackRepo;
import com.bhushan.spring.files.excel.repository.LoginRepo;
import com.bhushan.spring.files.excel.repository.OfflineCustomerDetailsRepo;
import com.bhushan.spring.files.excel.repository.PaymentHistoryRepo;
import com.bhushan.spring.files.excel.repository.ProductDetailsDataRepo;
import com.bhushan.spring.files.excel.repository.ProductPurchasedRepo;
import com.bhushan.spring.files.excel.repository.ProductRepository;
import com.bhushan.spring.files.excel.repository.PurchaseDetailsRepo;
import com.bhushan.spring.files.excel.repository.ServiceFeedbackFormRepo;
import com.bhushan.spring.files.excel.response.ProdResponse;

@Service
public class EmployeeService {

	@Autowired
	ProductRepository repository;

	@Autowired
	OnlineCustomerRequestRepo customerProductSaveRepo;
	@Autowired
	PaymentHistoryRepo paymentHistoryRepo;
	@Autowired
	ProductPurchasedRepo productpurchasedrepo;
	@Autowired
	PurchaseDetailsRepo purchasedetailsrepo;
	@Autowired
	FeedbackRepo feedbackrepo;
	@Autowired
	ServiceFeedbackFormRepo feedbackreposrepo;
	@Autowired
	ProductDetailsDataRepo productDetailsDataRepo;
	@Autowired
	private EmployeeDetailsRepo employeeDetailsRepo;
	@Autowired
	private OfflineCustomerDetailsRepo offlinecustomerdetailsrepo;
	@Autowired
	private LoginRepo loginRepo;
	@Autowired
	private MessageUtil messageUtill;

	public Optional<EmployeeEntity> saveDatas(EmployeeEntity response) throws ClientProtocolException, IOException {
		LoginEntity loginEntity = new LoginEntity();
		String timeStamp = new SimpleDateFormat("dd-MM-yyyy").format(Calendar.getInstance().getTime());
		response.setJoineddate(timeStamp);
		loginEntity.setUsername(response.getAdharno());
		loginEntity.setPassword(generateOTP());
		loginEntity.setName(response.getName());
		LoginEntity save2 = loginRepo.save(loginEntity);
		response.setUsername(response.getAdharno());
		response.setPassword(save2.getPassword());
		EmployeeEntity save = employeeDetailsRepo.save(response);
		messageUtill.sendMsgToEmployee(save);
		return employeeDetailsRepo.findById(save.getId());
	}

	public List<EmployeeEntity> getallemployees() {
		List<EmployeeEntity> findAll = employeeDetailsRepo.findAll();
		return findAll;
	}

	public List<String> getallemployeesName() {
		List<EmployeeEntity> findAll = employeeDetailsRepo.findAll();
		ArrayList<String> arrayList = new ArrayList<>();
		for (int i = 0; i < findAll.size(); i++) {
			arrayList.add(findAll.get(i).getName());
		}
		return arrayList;
	}

	public ProdResponse deleteemployee(int id) throws Exception {
		ProdResponse employeeResponse = new ProdResponse();
		Optional<EmployeeEntity> findById = employeeDetailsRepo.findById(id);
		if (findById.isPresent()) {
			employeeDetailsRepo.deleteById(id);
			employeeResponse.setMessage("The Employee has been deleted");
			employeeResponse.setStatus("200");
		} else {
			employeeResponse.setMessage("The EmployeeId is not Present");
			employeeResponse.setStatus("500");
		}
		return employeeResponse;
	}

	public EmployeeEntity findbyemployeeid(int id) throws Exception {
		Optional<EmployeeEntity> findById = employeeDetailsRepo.findById(id);
		if (findById.isPresent()) {
			EmployeeEntity employeeEntity = findById.get();
			return employeeEntity;
		} else {
			throw new Exception("EmployeeId is not Present");
		}
	}

	public static String generateOTP() {
		int randomPin = (int) (Math.random() * 9000) + 1000;
		String otp = String.valueOf(randomPin);
		return otp;
	}
}
