package com.bhushan.spring.files.excel.service;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.http.client.ClientProtocolException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bhushan.spring.files.excel.helper.ExcelHelper;
import com.bhushan.spring.files.excel.model.EmployeeEntity;
import com.bhushan.spring.files.excel.model.ExcelSheetToDBProductDetails;
import com.bhushan.spring.files.excel.model.FeedbackForm;
import com.bhushan.spring.files.excel.model.LoginEntity;
import com.bhushan.spring.files.excel.model.OfflineCustomerDetails;
import com.bhushan.spring.files.excel.model.PaymentHistory;
import com.bhushan.spring.files.excel.model.OnlineCustomerRequest;
import com.bhushan.spring.files.excel.model.PurchasedProduct;
import com.bhushan.spring.files.excel.model.ServiceFeedbackForm;
import com.bhushan.spring.files.excel.repository.OnlineCustomerRequestRepo;
import com.bhushan.spring.files.excel.repository.EmployeeDetailsRepo;
import com.bhushan.spring.files.excel.repository.FeedbackRepo;
import com.bhushan.spring.files.excel.repository.LoginRepo;
import com.bhushan.spring.files.excel.repository.OfflineCustomerDetailsRepo;
import com.bhushan.spring.files.excel.repository.PaymentHistoryRepo;
import com.bhushan.spring.files.excel.repository.ProductDetailsDataRepo;
import com.bhushan.spring.files.excel.repository.ProductPurchasedRepo;
import com.bhushan.spring.files.excel.repository.ProductRepository;
import com.bhushan.spring.files.excel.repository.PurchaseDetailsRepo;
import com.bhushan.spring.files.excel.repository.ServiceFeedbackFormRepo;
import com.bhushan.spring.files.excel.response.CustomerWarrantyRequest;
import com.bhushan.spring.files.excel.response.CustomerWarrantyResponse;
import com.bhushan.spring.files.excel.response.OfflineCustomerResponse;
import com.bhushan.spring.files.excel.response.OnlineCustomerResponse;
import com.bhushan.spring.files.excel.response.ProdResponse;

@Service
public class ExcelService {
	@Autowired
	ProductRepository repository;

	@Autowired
	OnlineCustomerRequestRepo customerProductSaveRepo;
	@Autowired
	PaymentHistoryRepo paymentHistoryRepo;
	@Autowired
	ProductPurchasedRepo productpurchasedrepo;
	@Autowired
	PurchaseDetailsRepo purchasedetailsrepo;
	@Autowired
	FeedbackRepo feedbackrepo;
	@Autowired
	ServiceFeedbackFormRepo feedbackreposrepo;
	@Autowired
	ProductDetailsDataRepo productDetailsDataRepo;
	@Autowired
	private EmployeeDetailsRepo employeeDetailsRepo;
	@Autowired
	private OfflineCustomerDetailsRepo offlinecustomerdetailsrepo;
	@Autowired
	private LoginRepo loginRepo;
	@Autowired
	private MessageUtil messageUtill;

	public void save() throws FileNotFoundException {
		File file = new File("/home/moglix/Downloads/Test.xlsx");
		InputStream targetStream = new FileInputStream(file);
		List<ExcelSheetToDBProductDetails> tutorials = ExcelHelper.excelToTutorials(targetStream);
		repository.saveAll(tutorials);
	}

	public OnlineCustomerRequest findbyids(int id) throws Exception {
		System.out.println("Id is" + id);
		Optional<OnlineCustomerRequest> findById = customerProductSaveRepo.findById(id);
		System.out.println(findById.get());
		if (findById.isPresent()) {
			OnlineCustomerRequest test = findById.get();
			return test;
		} else {
			throw new Exception("Customer not Found Exception");
		}

	}

	public OnlineCustomerRequest findbycustomeridandmobileno(int customerid, long mobileno) throws Exception {
		OnlineCustomerRequest findbycustomeridandmobileno = customerProductSaveRepo
				.findbycustomeridandmobileno(customerid, mobileno);
		return findbycustomeridandmobileno;
	}

	public FeedbackForm saveData(FeedbackForm feedbackForm) throws Exception {
		Optional<OnlineCustomerRequest> findById = customerProductSaveRepo.findById(feedbackForm.getCustomerid());
		if (findById.isPresent()) {
			FeedbackForm save = feedbackrepo.save(feedbackForm);
			return save;
		} else {
			throw new Exception("Record not Found");
		}

	}

	public ServiceFeedbackForm saveData(ServiceFeedbackForm feedbackForm) throws Exception {
		Optional<OnlineCustomerRequest> findById = customerProductSaveRepo.findById(feedbackForm.getCustomerid());
		if (findById.isPresent()) {
			ServiceFeedbackForm save = feedbackreposrepo.save(feedbackForm);
			return save;
		} else {
			throw new Exception("Record not Found");
		}

	}

	public FeedbackForm getFeedbacks(int customerid) {
		FeedbackForm feedbackrepos = feedbackrepo.findbycustomerid(customerid);
		return feedbackrepos;
	}

	public ServiceFeedbackForm getFeedback(int customerid) {
		ServiceFeedbackForm feedbackrepos = feedbackreposrepo.findbycustomerid(customerid);
		return feedbackrepos;
	}

	public List<OnlineCustomerRequest> getListOfData() {
		List<OnlineCustomerRequest> findAll = customerProductSaveRepo.findAll();
		return findAll;
	}

	
	public Optional<OnlineCustomerRequest> saveData(OnlineCustomerRequest response)
			throws ClientProtocolException, IOException {
		String generatedString = RandomStringUtils.randomNumeric(6);
		String timeStamp = new SimpleDateFormat("dd-MM-yyyy").format(Calendar.getInstance().getTime());
		response.getPurchasedetails().setPurchasedate(timeStamp);
		int parseInt = Integer.parseInt(generatedString);
		response.setInvoiceno(parseInt);
		for (int i = 0; i < response.getProductpurchased().size(); i++) {
			response.getProductpurchased().get(i).setDateofpurchase(timeStamp);
			response.getProductpurchased().get(i).setTypeofcustomer("ONLINE_CUSTOMER");
		}
		OnlineCustomerRequest save = customerProductSaveRepo.save(response);
		messageUtill.sendOnlineCustomerDetails(save);
		messageUtill.sendOnlineCustomerDetailstoadmin(save);
		return customerProductSaveRepo.findById(save.getId());
	}

	public OfflineCustomerDetails findofflinecustomerbyid(int id) {
		Optional<OfflineCustomerDetails> findById = offlinecustomerdetailsrepo.findById(id);
		return findById.get();
	}

	public List<OfflineCustomerDetails> getallofflinecustomer() {
		List<OfflineCustomerDetails> findById = offlinecustomerdetailsrepo.findAll();
		return findById;
	}

	public OnlineCustomerRequest updateData(OnlineCustomerRequest response) throws Exception {
		String timeStamp = new SimpleDateFormat("dd-MM-yyyy").format(Calendar.getInstance().getTime());
		response.getPurchasedetails().setPurchasedate(timeStamp);
		Optional<OnlineCustomerRequest> findById = customerProductSaveRepo.findById(response.getId());
		if (findById.isPresent()) {
			PurchasedProduct productPurchased = new PurchasedProduct();
			PaymentHistory paymentHistory = new PaymentHistory();
			OnlineCustomerRequest productPurchasedSaveDetails = findById.get();
			productPurchasedSaveDetails.setAddress(response.getAddress());
			productPurchasedSaveDetails.setMailid(response.getMailid());
			productPurchasedSaveDetails.setMobileno(response.getMobileno());
			productPurchasedSaveDetails.setName(response.getName());
			if (response.isIs_cancelled() == true) {
				productPurchasedSaveDetails.setIs_cancelled(response.isIs_cancelled());
				productPurchasedSaveDetails.setCancelleddate(timeStamp);
				messageUtill.orderCancellation(productPurchasedSaveDetails);
				messageUtill.orderCancellationAdmin(productPurchasedSaveDetails);
			}
			paymentHistory.setDateofdelivery(response.getPaymenthistory().getDateofdelivery());
			paymentHistory.setOrderstatus(response.getPaymenthistory().getOrderstatus());
			paymentHistory.setPaymentdate(response.getPaymenthistory().getPaymentdate());
			paymentHistory.setPaymentstatus(response.getPaymenthistory().getPaymentstatus());
			paymentHistory.setTransactionid(response.getPaymenthistory().getTransactionid());
			paymentHistory.setDeliverycharges(response.getPaymenthistory().getDeliverycharges());
			if (paymentHistory.getDeliverycharges() == 0) {
				paymentHistory.setTotalcostAfterapplyingdeliverycharges(
						productPurchasedSaveDetails.getPurchasedetails().getTotalamount());
			} else {
				paymentHistory.setTotalcostAfterapplyingdeliverycharges(paymentHistory.getDeliverycharges()
						+ productPurchasedSaveDetails.getPurchasedetails().getTotalamount());
			}
//			productPurchased.setWarrantydays(response.getProductpurchased().get(0).getWarrantydays());
			productPurchasedSaveDetails.setPaymenthistory(paymentHistory);
			productPurchasedSaveDetails.setPostalcode(response.getPostalcode());
			productPurchasedSaveDetails.setProductpurchased(response.getProductpurchased());
			productPurchasedSaveDetails.setPurchasedetails(response.getPurchasedetails());
			productPurchasedSaveDetails = customerProductSaveRepo.save(productPurchasedSaveDetails);
			if (productPurchasedSaveDetails.isIs_cancelled() != true) {
				messageUtill.onlineOrderValueUpdateByAdmin(productPurchasedSaveDetails);
			}
			return productPurchasedSaveDetails;
		} else {
			throw new Exception("The Id is not Present");
		}
	}

	public List<ExcelSheetToDBProductDetails> getData(List<Integer> productid) {
		List<ExcelSheetToDBProductDetails> findAllByProductIdIn = repository.findAllByproductidIn(productid);
		for (int i = 0; i < findAllByProductIdIn.size(); i++) {
			int frequency = Collections.frequency(productid, findAllByProductIdIn.get(i).getProductid());
			findAllByProductIdIn.get(i).setQuantity(frequency);
			int quantity = findAllByProductIdIn.get(i).getQuantity();
			String purchaseamount = findAllByProductIdIn.get(i).getPurchaseamount();
			findAllByProductIdIn.get(i).setTotalamount(quantity * Integer.parseInt(purchaseamount));
		}
		return findAllByProductIdIn;
	}

	public List<ExcelSheetToDBProductDetails> getListOfData(List<Integer> productid) {
		List<ExcelSheetToDBProductDetails> findAllByProductIdIn = repository.findAllByproductidIn(productid);
		return findAllByProductIdIn;
	}

	public static String dateofexpiry(String dateofdelivery, String warrantydays) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		c.add(Calendar.DATE, Integer.parseInt(warrantydays));
		String output = sdf.format(c.getTime());
		return output;
	}

	public List<ExcelSheetToDBProductDetails> getDetailsOfProduct(List<Integer> productid) {
		List<ExcelSheetToDBProductDetails> arrayList = new ArrayList<ExcelSheetToDBProductDetails>();
		for (int prodid : productid) {
			Optional<ExcelSheetToDBProductDetails> findById = productDetailsDataRepo.findById(prodid);
			if (findById.isPresent()) {
				arrayList.add(findById.get());
			}
		}
		System.out.println(arrayList);
		return arrayList;
	}

	public double getTotalSumOfTheProduct(List<Integer> productid) {
		double sum = 0;
		List<Integer> findpricebyproductid = repository.findpricebyproductid(productid);
		for (int i = 0; i < findpricebyproductid.size(); i++) {
			sum += findpricebyproductid.get(i);
		}
		return sum;
	}

	public double getTotalSumOfTheProducts(List<Integer> productid) {
		int cost = 0;
		List<ExcelSheetToDBProductDetails> data = getData(productid);
		for (int i = 0; i < data.size(); i++) {
			int totalamount = data.get(i).getTotalamount();
			cost += totalamount;
		}
		return cost;
	}

	public CustomerWarrantyResponse getDetails(CustomerWarrantyRequest customerWarrantyRequest) throws Exception {
		CustomerWarrantyResponse customerWarrantyResponse = new CustomerWarrantyResponse();
		Optional<OnlineCustomerRequest> customer = customerProductSaveRepo
				.findById(customerWarrantyRequest.getCustomerid());
		LocalDate purchasedDate = LocalDate.parse(customerWarrantyRequest.getDeliverydate());
		LocalDate warrantyDate = purchasedDate.plusDays(180);
		LocalDate now = LocalDate.now();
		Long range = ChronoUnit.DAYS.between(now, warrantyDate);
		if (now.compareTo(warrantyDate) < 0) {
			customerWarrantyResponse.setStatus("200");
			customerWarrantyResponse.setMessage("Days left for expiry " + range);
			customerWarrantyResponse.setWarrantyapplicable(true);
		} else {
			customerWarrantyResponse.setStatus("500");
			customerWarrantyResponse.setMessage("Warranty has been expired" + " " + range + " " + "Days ago");
			customerWarrantyResponse.setWarrantyapplicable(false);
		}
		return customerWarrantyResponse;
	}

	public ByteArrayInputStream load() {
		List<ExcelSheetToDBProductDetails> tutorials = repository.findAll();
		ByteArrayInputStream in = ExcelHelper.tutorialsToExcel(tutorials);
		return in;
	}

	public List<ExcelSheetToDBProductDetails> getAllTutorials() {
		return repository.findAll();
	}

	public OfflineCustomerResponse findofflinecustomerbyinvoiceandmobileno(int invoiceno, long mobileno) {
		OfflineCustomerResponse offlineCustomerResponse = new OfflineCustomerResponse();
		Optional<OfflineCustomerDetails> findbyinvoiceandmobile = offlinecustomerdetailsrepo
				.findbyinvoiceandmobile(invoiceno, mobileno);
		if (findbyinvoiceandmobile.isPresent()) {
			offlineCustomerResponse.setMessage("Data Retrieved");
			offlineCustomerResponse.setStatus("200");
			offlineCustomerResponse.setOfflineCustomerDetails(findbyinvoiceandmobile.get());
		} else {
			offlineCustomerResponse.setMessage("Data not Available");
			offlineCustomerResponse.setStatus("500");
		}
		return offlineCustomerResponse;
	}

	public OnlineCustomerResponse findonlinecustomerbyinvoiceandmobileno(int invoiceno, long mobileno) {
		OnlineCustomerResponse onlinecustomerresponse = new OnlineCustomerResponse();
		Optional<OnlineCustomerRequest> findbyinvoiceandmobile = customerProductSaveRepo
				.findbyinvoiceandmobileno(invoiceno, mobileno);
		if (findbyinvoiceandmobile.isPresent()) {
			onlinecustomerresponse.setMessage("Data Retrieved");
			onlinecustomerresponse.setStatus("200");
			onlinecustomerresponse.setProductpurchaseddetails(findbyinvoiceandmobile.get());
		} else {
			onlinecustomerresponse.setMessage("Data not Available");
			onlinecustomerresponse.setStatus("500");
		}
		return onlinecustomerresponse;
	}
}
