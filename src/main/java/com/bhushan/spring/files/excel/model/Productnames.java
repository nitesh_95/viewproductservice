package com.bhushan.spring.files.excel.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "productnames")
public class Productnames {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	private double purchaseamount;
	private int quantity;
	private int productid;
	private String imagepath;
	private String productname;
	private String description;
	private String warrantydays;
	private double unitprice;
	private double totalamount;
	private String dateofpurchase;
	private String typeofcustomer;

	public String getTypeofcustomer() {
		return typeofcustomer;
	}

	public void setTypeofcustomer(String typeofcustomer) {
		this.typeofcustomer = typeofcustomer;
	}

	public String getDateofpurchase() {
		return dateofpurchase;
	}

	public void setDateofpurchase(String dateofpurchase) {
		this.dateofpurchase = dateofpurchase;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getPurchaseamount() {
		return purchaseamount;
	}

	public void setPurchaseamount(double purchaseamount) {
		this.purchaseamount = purchaseamount;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getProductid() {
		return productid;
	}

	public void setProductid(int productid) {
		this.productid = productid;
	}

	public String getImagepath() {
		return imagepath;
	}

	public void setImagepath(String imagepath) {
		this.imagepath = imagepath;
	}

	public String getProductname() {
		return productname;
	}

	public void setProductname(String productname) {
		this.productname = productname;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getWarrantydays() {
		return warrantydays;
	}

	public void setWarrantydays(String warrantydays) {
		this.warrantydays = warrantydays;
	}

	public double getUnitprice() {
		return unitprice;
	}

	public void setUnitprice(double unitprice) {
		this.unitprice = unitprice;
	}

	public double getTotalamount() {
		return totalamount;
	}

	public void setTotalamount(double totalamount) {
		this.totalamount = totalamount;
	}

}
