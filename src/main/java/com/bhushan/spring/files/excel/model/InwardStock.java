package com.bhushan.spring.files.excel.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "inwardstock")
public class InwardStock {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	private String dateofpurchase;
	private String purchasefrom;
	private int quantity;
	private double unitprice;
	private double inventory;
	private float taxrate;
	@Transient
	private int remainingstock;
	@ManyToOne(cascade = { CascadeType.ALL })
	private StockProduct addProductInStock;
	@ManyToOne(cascade = { CascadeType.ALL })
	private TypeOfCustomer typeofcustomer;
	@ManyToOne(cascade = { CascadeType.ALL })
	private TypeOfPrice typeofprice;
	
	
	public int getRemainingstock() {
		return remainingstock;
	}

	public void setRemainingstock(int remainingstock) {
		this.remainingstock = remainingstock;
	}

	public float getTaxrate() {
		return taxrate;
	}

	public void setTaxrate(float taxrate) {
		this.taxrate = taxrate;
	}

	public double getInventory() {
		return inventory;
	}

	public void setInventory(double inventory) {
		this.inventory = inventory;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDateofpurchase() {
		return dateofpurchase;
	}

	public void setDateofpurchase(String dateofpurchase) {
		this.dateofpurchase = dateofpurchase;
	}

	public String getPurchasefrom() {
		return purchasefrom;
	}

	public void setPurchasefrom(String purchasefrom) {
		this.purchasefrom = purchasefrom;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public double getUnitprice() {
		return unitprice;
	}

	public void setUnitprice(double uniprice) {
		this.unitprice = uniprice;
	}

	public StockProduct getAddProductInStock() {
		return addProductInStock;
	}

	public void setAddProductInStock(StockProduct addProductInStock) {
		this.addProductInStock = addProductInStock;
	}

	public TypeOfCustomer getTypeofcustomer() {
		return typeofcustomer;
	}

	public void setTypeofcustomer(TypeOfCustomer typeofcustomer) {
		this.typeofcustomer = typeofcustomer;
	}

	public TypeOfPrice getTypeofprice() {
		return typeofprice;
	}

	public void setTypeofprice(TypeOfPrice typeofprice) {
		this.typeofprice = typeofprice;
	}

}
