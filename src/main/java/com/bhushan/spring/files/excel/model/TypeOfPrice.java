package com.bhushan.spring.files.excel.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "typeofprice")
public class TypeOfPrice {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	private double technicianprice;
	private double sellingprice;
	private double exactprice;
	private double onlineprice;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public double getTechnicianprice() {
		return technicianprice;
	}
	public void setTechnicianprice(double technicianprice) {
		this.technicianprice = technicianprice;
	}
	public double getSellingprice() {
		return sellingprice;
	}
	public void setSellingprice(double sellingprice) {
		this.sellingprice = sellingprice;
	}
	public double getExactprice() {
		return exactprice;
	}
	public void setExactprice(double exactprice) {
		this.exactprice = exactprice;
	}
	public double getOnlineprice() {
		return onlineprice;
	}
	public void setOnlineprice(double onlineprice) {
		this.onlineprice = onlineprice;
	}

}
