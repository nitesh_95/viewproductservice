package com.bhushan.spring.files.excel.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "emipayments")
public class Emipayment {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int emipaymentid;
	private double amount;
	private double paidamount;
	private double remainingbalance;
	private String date;
	private boolean emipaymentEnded;
	
	public int getEmipaymentid() {
		return emipaymentid;
	}

	public void setEmipaymentid(int emipaymentid) {
		this.emipaymentid = emipaymentid;
	}

	public boolean isEmipaymentEnded() {
		return emipaymentEnded;
	}

	public void setEmipaymentEnded(boolean emipaymentEnded) {
		this.emipaymentEnded = emipaymentEnded;
	}

	public double getPaidamount() {
		return paidamount;
	}

	public void setPaidamount(double paidamount) {
		this.paidamount = paidamount;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public double getRemainingbalance() {
		return remainingbalance;
	}

	public void setRemainingbalance(double remainingbalance) {
		this.remainingbalance = remainingbalance;
	}

	public int getId() {
		return emipaymentid;
	}

	public void setId(int emipaymentid) {
		this.emipaymentid = emipaymentid;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

}
