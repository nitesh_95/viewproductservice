package com.bhushan.spring.files.excel.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "productpurchasedata")
public class OnlineCustomerRequest {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "customerid")
	private int id;
	private String name;
	private String mailid;
	private String address;
	private String postalcode;
	private long mobileno;
	private int invoiceno;
	private boolean is_cancelled;
	private String cancelleddate;
	@ManyToOne(cascade = { CascadeType.ALL })
	private PurchaseDetails purchasedetails;
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "customerid")
	private List<Productnames> productpurchased;
	@ManyToOne(cascade = { CascadeType.ALL })
	private PaymentHistory paymenthistory;
	
	public int getInvoiceno() {
		return invoiceno;
	}

	public void setInvoiceno(int invoiceno) {
		this.invoiceno = invoiceno;
	}

	public String getCancelleddate() {
		return cancelleddate;
	}

	public void setCancelleddate(String cancelleddate) {
		this.cancelleddate = cancelleddate;
	}

	public boolean isIs_cancelled() {
		return is_cancelled;
	}

	public void setIs_cancelled(boolean is_cancelled) {
		this.is_cancelled = is_cancelled;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMailid() {
		return mailid;
	}

	public void setMailid(String mailid) {
		this.mailid = mailid;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPostalcode() {
		return postalcode;
	}

	public void setPostalcode(String postalcode) {
		this.postalcode = postalcode;
	}

	public long getMobileno() {
		return mobileno;
	}

	public void setMobileno(long mobileno) {
		this.mobileno = mobileno;
	}

	public PurchaseDetails getPurchasedetails() {
		return purchasedetails;
	}

	public void setPurchasedetails(PurchaseDetails purchasedetails) {
		this.purchasedetails = purchasedetails;
	}

	public List<Productnames> getProductpurchased() {
		return productpurchased;
	}

	public void setProductpurchased(List<Productnames> productpurchased) {
		this.productpurchased = productpurchased;
	}

	public PaymentHistory getPaymenthistory() {
		return paymenthistory;
	}

	public void setPaymenthistory(PaymentHistory paymenthistory) {
		this.paymenthistory = paymenthistory;
	}

}