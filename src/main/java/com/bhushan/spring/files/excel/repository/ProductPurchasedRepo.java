package com.bhushan.spring.files.excel.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bhushan.spring.files.excel.model.ProductPurchased;

@Repository
public interface ProductPurchasedRepo extends JpaRepository<ProductPurchased, Integer> {

	@Query(value = "select * from productpurchased where dateofpurchase =?1", nativeQuery = true)
	List<ProductPurchased> getbydates(@Param(value = "dateofpurchase") String dateofpurchase);

}
