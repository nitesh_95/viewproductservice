package com.bhushan.spring.files.excel.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "adminservicingconfirmation")
public class AdminServicingconfirmation {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	private double amount;
	private boolean servicingaccepted;
	private String dateofservicing;
	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public boolean isServicingaccepted() {
		return servicingaccepted;
	}

	public void setServicingaccepted(boolean servicingaccepted) {
		this.servicingaccepted = servicingaccepted;
	}

	public String getDateofservicing() {
		return dateofservicing;
	}

	public void setDateofservicing(String dateofservicing) {
		this.dateofservicing = dateofservicing;
	}
}
