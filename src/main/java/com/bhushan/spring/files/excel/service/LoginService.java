package com.bhushan.spring.files.excel.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bhushan.spring.files.excel.model.LoginEntity;
import com.bhushan.spring.files.excel.model.ServicingEntity;
import com.bhushan.spring.files.excel.repository.LoginRepo;
import com.bhushan.spring.files.excel.response.LoginResponse;

@Service
public class LoginService {

	@Autowired
	private LoginRepo loginRepo;
	@Autowired
	private Servicingdetailsservice servicingdetailsservice;

	public LoginResponse getTechnicianLogin(Long username, String password) {
		LoginResponse loginResponse = new LoginResponse();
		Optional<LoginEntity> loginEntity = loginRepo.findbyusername(username);
		if (loginEntity.isPresent() && loginEntity.get().getPassword().equals(password)) {
			loginResponse.setStatus("200");
			loginResponse.setMessage("User Is Authenticated");
			loginResponse.setName(loginEntity.get().getName());
			List<ServicingEntity> findall = servicingdetailsservice.findall();
			List<ServicingEntity> collect = findall.stream()
					.filter(i -> loginEntity.get().getName().equals(i.getAssignedto())).collect(Collectors.toList());
			if (collect != null) {
				loginResponse.setServicingEntities(collect);
				loginResponse.setLoginEntity(loginEntity.get());
			}
		} else {
			loginResponse.setStatus("400");
			loginResponse.setMessage("User Is not Authenticated");
		}
		return loginResponse;
	}

	public List<LoginEntity> findall() {
		List<LoginEntity> findAll = loginRepo.findAll();
		return findAll;
	}

	public LoginResponse getAdminLogin(String username, String password) {
		LoginResponse loginResponse = new LoginResponse();
		Optional<LoginEntity> loginEntity = loginRepo.findById(username);
		if (loginEntity.isPresent()) {
			loginResponse.setStatus("200");
			loginResponse.setMessage("User Is Authenticated");
			loginResponse.setName(loginEntity.get().getName());
			loginResponse.setLoginEntity(loginEntity.get());
		} else {
			loginResponse.setStatus("400");
			loginResponse.setMessage("User Is not Authenticated");
		}
		return loginResponse;
	}

}
