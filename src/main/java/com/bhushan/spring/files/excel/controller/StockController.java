package com.bhushan.spring.files.excel.controller;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bhushan.spring.files.excel.model.InwardStock;
import com.bhushan.spring.files.excel.model.ProductPurchased;
import com.bhushan.spring.files.excel.model.Productnames;
import com.bhushan.spring.files.excel.model.StockProduct;
import com.bhushan.spring.files.excel.response.ProdResponse;
import com.bhushan.spring.files.excel.service.StockProductService;
import com.bhushan.spring.files.excel.util.ProductUtil;

@RestController
@CrossOrigin
@RequestMapping("/api/excel")
public class StockController {

	@Autowired
	private StockProductService stockProductService;

	@PostMapping("/saveStockProduct")
	public StockProduct addProductInStock(@RequestBody StockProduct addProductInStock) {
		StockProduct saveData = stockProductService.saveData(addProductInStock);
		return saveData;
	}

	@PostMapping("/saveInwardStock")
	public InwardStock saveData(@RequestBody InwardStock inwardStock) {
		InwardStock saveData = stockProductService.saveInwardStock(inwardStock);
		return saveData;
	}

	@GetMapping("/getallstock")
	public List<InwardStock> findallstock() {
		return stockProductService.getallinwardstock();
	}

	@GetMapping("/getallstock/{id}")
	public ProdResponse deleteData(@PathVariable int id) {
		return stockProductService.deleteData(id);
	}

	@GetMapping("/getallstockproductnames")
	public List<StockProduct> stockProduct() {
		return stockProductService.getallstockproductname();
	}

	@GetMapping("/getproductnamequantityOnline")
	public Map<String, Integer> getvaluesfrommap() throws SQLException {
		ProductUtil productUtil = new ProductUtil();
		return productUtil.sensorValuesMap();
	}

	@GetMapping("/getproductnamequantityOffline")
	public Map<String, Integer> getvaluesfrommaps() throws SQLException {
		ProductUtil productUtil = new ProductUtil();
		return productUtil.sensorValuesMaps();
	}

	@GetMapping("/onlineproductpurchasedstock")
	public List<Productnames> getproductpurchasedlist() {
		List<Productnames> getproducts = stockProductService.getproducts();
		return getproducts.stream().filter(i -> "ONLINE_CUSTOMER".equals(i.getTypeofcustomer()))
				.collect(Collectors.toList());

	}

	public HashMap<String, Integer> minusstockdata() {
		int quantities = 0;
		HashMap<String, Integer> hashMap = new HashMap<>();
		List<InwardStock> getallinwardstock = stockProductService.getallinwardstock();
		HashMap<String, Integer> getproductpurchasedlists = getproductpurchasedlists();
		for (int i = 0; i < getallinwardstock.size(); i++) {
			hashMap.put(getallinwardstock.get(i).getAddProductInStock().getItemname(),
					getallinwardstock.get(i).getQuantity());
		}
		return hashMap;
	}

	@GetMapping("/actualstockdetails")
	public List<Entry<String, Integer>> getstockproductdata() {
		int count = 0;
		List<Entry<String, Integer>> getstockproductdata = stockProductService.getstockproductdata();
		return getstockproductdata;
	}

	@GetMapping("/onlineproductpurchasedstockdata")
	public HashMap<String, Integer> getproductpurchasedlists() {
		int quantities = 0;
		HashMap<String, Integer> hashMap = new HashMap<>();
		List<Productnames> getbydate = stockProductService.getproducts();
		for (int i = 0; i < getbydate.size(); i++) {
			hashMap.put(getbydate.get(i).getProductname(), getbydate.get(i).getQuantity());
		}
		for (int i = 0; i < getbydate.size() - 1; i++) {
			for (int k = i + 1; k < getbydate.size(); k++) {
				if (getbydate.get(i).getProductname().equalsIgnoreCase(getbydate.get(k).getProductname())) {
					int collectx = getbydate.get(i).getQuantity();
					int collects = getbydate.get(k).getQuantity();
					quantities = collects + collectx;
					hashMap.put(getbydate.get(i).getProductname(), quantities);
				}
			}
		}
		return hashMap;
	}

	@GetMapping("/offlineproductcustomerdetails")
	public List<ProductPurchased> productpurchaseddetails() {
		return stockProductService.getproductsdata();
	}

	@GetMapping("/offlineproductcustomerdetails/{dateofpurchase}")
	public List<ProductPurchased> getbydates(@PathVariable String dateofpurchase) throws ParseException {
		DateFormat inputFormat = new SimpleDateFormat("E MMM dd yyyy HH:mm:ss 'GMT'z", Locale.ENGLISH);
		Date date = inputFormat.parse(dateofpurchase);
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
		System.out.println(formatter.format(date));
		return stockProductService.getbydates(formatter.format(date));
	}

	@GetMapping("/offlineproductpurchase/{dateofpurchase}")
	public HashMap<String, Integer> getdata(@PathVariable String dateofpurchase) throws ParseException {
		Integer quantities = 0;
		HashMap<String, Integer> hashMap = new HashMap<>();
		DateFormat inputFormat = new SimpleDateFormat("E MMM dd yyyy HH:mm:ss 'GMT'z", Locale.ENGLISH);
		Date date = inputFormat.parse(dateofpurchase);
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
		System.out.println(formatter.format(date));
		List<ProductPurchased> getbydate = stockProductService.getbydates(formatter.format(date));
		for (int i = 0; i < getbydate.size(); i++) {
			hashMap.put(getbydate.get(i).getProductnamesdata(), getbydate.get(i).getQuantities());
		}
		for (int i = 0; i < getbydate.size() - 1; i++) {
			for (int k = i + 1; k < getbydate.size(); k++) {
				if (getbydate.get(i).getProductnamesdata().equalsIgnoreCase(getbydate.get(k).getProductnamesdata())) {
					int collectx = getbydate.get(i).getQuantities();
					int collects = getbydate.get(k).getQuantities();
					quantities = collects + collectx;
					hashMap.put(getbydate.get(i).getProductnamesdata(), quantities);
				}
			}
		}
		return hashMap;
	}

	@GetMapping("/onlineproductpurchasedstocks/{dateofpurchase}")
	public HashMap<String, Integer> getdatas(@PathVariable String dateofpurchase) throws ParseException {
		Integer quantities = 0;
		HashMap<String, Integer> hashMap = new HashMap<>();
		DateFormat inputFormat = new SimpleDateFormat("E MMM dd yyyy HH:mm:ss 'GMT'z", Locale.ENGLISH);
		Date date = inputFormat.parse(dateofpurchase);
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
		System.out.println(formatter.format(date));
		List<Productnames> getbydate = stockProductService.getbydate(formatter.format(date));
		List<Productnames> collect = getbydate.stream().filter(i -> i.getTypeofcustomer().equals("ONLINE_CUSTOMER"))
				.collect(Collectors.toList());
		for (int i = 0; i < collect.size(); i++) {
			hashMap.put(collect.get(i).getProductname(), collect.get(i).getQuantity());
		}
		for (int i = 0; i < collect.size() - 1; i++) {
			for (int k = i + 1; k < collect.size(); k++) {
				if (collect.get(i).getProductname().equalsIgnoreCase(collect.get(k).getProductname())) {
					int collectx = collect.get(i).getQuantity();
					int collects = collect.get(k).getQuantity();
					quantities = collects + collectx;
					hashMap.put(collect.get(i).getProductname(), quantities);
				}
			}
		}
		return hashMap;
	}

	@GetMapping("/onlineproductpurchasedstock/{dateofpurchase}")
	public List<Productnames> getbydate(@PathVariable String dateofpurchase) throws ParseException {
		DateFormat inputFormat = new SimpleDateFormat("E MMM dd yyyy HH:mm:ss 'GMT'z", Locale.ENGLISH);
		Date date = inputFormat.parse(dateofpurchase);
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
		System.out.println(formatter.format(date));
		List<Productnames> getbydate = stockProductService.getbydate(formatter.format(date));
		List<Productnames> collect = getbydate.stream().filter(i -> i.getTypeofcustomer().equals("ONLINE_CUSTOMER"))
				.collect(Collectors.toList());
		return collect;
	}
}
