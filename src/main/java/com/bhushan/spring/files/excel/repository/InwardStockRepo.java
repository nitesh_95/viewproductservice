package com.bhushan.spring.files.excel.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bhushan.spring.files.excel.model.InwardStock;

@Repository
public interface InwardStockRepo extends JpaRepository<InwardStock, Integer>{

}
