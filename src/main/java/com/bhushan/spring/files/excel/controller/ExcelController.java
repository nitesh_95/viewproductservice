package com.bhushan.spring.files.excel.controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.bhushan.spring.files.excel.message.ResponseMessage;
import com.bhushan.spring.files.excel.model.ExcelSheetToDBProductDetails;
import com.bhushan.spring.files.excel.model.FeedbackForm;
import com.bhushan.spring.files.excel.model.OfflineCustomerDetails;
import com.bhushan.spring.files.excel.model.OnlineCustomerRequest;
import com.bhushan.spring.files.excel.model.ServiceFeedbackForm;
import com.bhushan.spring.files.excel.response.CustomerWarrantyRequest;
import com.bhushan.spring.files.excel.response.CustomerWarrantyResponse;
import com.bhushan.spring.files.excel.response.OfflinCustomerDetailsResponse;
import com.bhushan.spring.files.excel.response.OfflineCustomerResponse;
import com.bhushan.spring.files.excel.response.OnlineCustomerResponse;
import com.bhushan.spring.files.excel.service.ExcelService;
import com.bhushan.spring.files.excel.service.OfflineCustomerDataService;

@RestController
@CrossOrigin
@RequestMapping("/api/excel")
public class ExcelController {

	@Autowired
	ExcelService fileService;
	@Autowired
	OfflineCustomerDataService offlinecustomerdataservice;

	@Autowired
	private RestTemplate restTemplate;

	@PostMapping("/saveData")
	public Optional<OnlineCustomerRequest> saveData(@RequestBody OnlineCustomerRequest response)
			throws ClientProtocolException, IOException {
		return fileService.saveData(response);
	}

	@PostMapping("/saveofflinecustomerdetails")
	public OfflinCustomerDetailsResponse saveData(@RequestBody OfflineCustomerDetails response)
			throws ClientProtocolException, IOException {
		return offlinecustomerdataservice.saveOfflineCustomerDetails(response);
	}

	@PostMapping("/updateofflinecustomer")
	public OfflinCustomerDetailsResponse updatedata(@RequestBody OfflineCustomerDetails response)
			throws ClientProtocolException, IOException {
		return offlinecustomerdataservice.updateData(response);
	}

	@GetMapping("/addressDetails/{pincode}")
	public boolean getemployees(@PathVariable int pincode) throws ParseException {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity<String> httpEntity = new HttpEntity<String>(httpHeaders);
		String body = restTemplate
				.exchange("https://api.worldpostallocations.com/?postalcode=" + pincode + "&countrycode=IN",
						HttpMethod.GET, httpEntity, String.class)
				.getBody();
		JSONObject jsonObject = new JSONObject(body);
		JSONArray configArrayData = jsonObject.getJSONArray("result");
		ArrayList<Object> arrayList = new ArrayList<>();
		for (int i = 0; i < configArrayData.length(); i++) {
			JSONObject jsonObject2 = configArrayData.getJSONObject(i);
			Object object = jsonObject2.get("state");
			arrayList.add(object);
		}
		boolean contains = arrayList.contains("Delhi");
		return contains;
	}

	@GetMapping("/offlinecustomerbyid/{id}")
	public OfflineCustomerDetails findofflinecustomer(@PathVariable int id) {
		OfflineCustomerDetails findofflinecustomerbyid = fileService.findofflinecustomerbyid(id);
		return findofflinecustomerbyid;
	}

	@GetMapping("/offlinecustomerbyid/{invoiceno}/{mobileno}")
	public OfflineCustomerResponse findofflinecustomer(@PathVariable int invoiceno, @PathVariable long mobileno) {
		OfflineCustomerResponse findofflinecustomerbyid = fileService.findofflinecustomerbyinvoiceandmobileno(invoiceno,
				mobileno);
		return findofflinecustomerbyid;
	}

	@GetMapping("/onlinecustomerid/{invoiceno}/{mobileno}")
	public OnlineCustomerResponse findonlinecustomer(@PathVariable int invoiceno, @PathVariable long mobileno) {
		OnlineCustomerResponse findonlinecustomerbyinvoiceandmobileno = fileService
				.findonlinecustomerbyinvoiceandmobileno(invoiceno, mobileno);
		return findonlinecustomerbyinvoiceandmobileno;
	}

	@GetMapping("/getofflinecustomerdetails")
	public List<OfflineCustomerDetails> findofflinecustomer() {
		List<OfflineCustomerDetails> findofflinecustomerbyid = fileService.getallofflinecustomer();
		return findofflinecustomerbyid;
	}

	@GetMapping("/getdata/{customerid}/{mobileno}")
	public OnlineCustomerRequest findbycustomeridandmobileno(@PathVariable int customerid,
			@PathVariable long mobileno) throws Exception {
		OnlineCustomerRequest findbycustomeridandmobileno = fileService.findbycustomeridandmobileno(customerid,
				mobileno);
		if (findbycustomeridandmobileno.getId() > 0) {
			return findbycustomeridandmobileno;
		} else {
			throw new Exception("Data is inValid");
		}
	}

	

	@PostMapping("/getListOfProducts")
	public List<ExcelSheetToDBProductDetails> getDetailsOfProduct(@RequestBody List<Integer> productid) {
		return fileService.getDetailsOfProduct(productid);
	}

	@GetMapping("/getfeedback/{customerid}")
	public FeedbackForm getfeedback(@PathVariable int customerid) {
		return fileService.getFeedbacks(customerid);
	}

	@GetMapping("/getfeedbackservice/{customerid}")
	public ServiceFeedbackForm getfeedbacks(@PathVariable int customerid) {
		return fileService.getFeedback(customerid);
	}

	@GetMapping("/getCustomerOnlineCustomerList")
	public List<OnlineCustomerRequest> getListOfData() {
		List<OnlineCustomerRequest> findAll = fileService.getListOfData();
		return findAll;
	}

	@PutMapping("/updatedata")
	public OnlineCustomerRequest updateData(@RequestBody OnlineCustomerRequest response) throws Exception {
		return fileService.updateData(response);

	}

	@PostMapping("/saveservicefeedback")
	public ServiceFeedbackForm saveData(@RequestBody ServiceFeedbackForm feedbackForm) throws Exception {
		return fileService.saveData(feedbackForm);
	}

	@PostMapping("/savefeedback")
	public FeedbackForm saveData(@RequestBody FeedbackForm feedbackForm) throws Exception {
		return fileService.saveData(feedbackForm);
	}

	@PostMapping("/getWarrantydetails")
	public CustomerWarrantyResponse getDetails(@RequestBody CustomerWarrantyRequest customerWarrantyRequest)
			throws Exception {
		return fileService.getDetails(customerWarrantyRequest);
	}

	@PostMapping("/uploadSheet")
	public ResponseEntity<ResponseMessage> uploadFile() throws FileNotFoundException {
		fileService.save();
		String message = "Data has been saved in Database";
		return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage(message));
	}

	@PostMapping("/getProductsId")
	public List<ExcelSheetToDBProductDetails> getListOfData(@RequestBody List<Integer> productid) {
		List<ExcelSheetToDBProductDetails> findAllByProductIdIn = fileService.getListOfData(productid);
		return findAllByProductIdIn;
	}

	
	@GetMapping("/getDetails/{id}")
	public OnlineCustomerRequest findbyids(@PathVariable int id) throws Exception {
		return fileService.findbyids(id);
	}

	@PostMapping("/getProductsIdList")
	public List<ExcelSheetToDBProductDetails> getListOfDatas(@RequestBody List<Integer> productid) {
		List<ExcelSheetToDBProductDetails> findAllByProductIdIn = fileService.getData(productid);
		return findAllByProductIdIn;
	}

	@PostMapping("/getTotalAmount")
	public double getTotalSumOfTheProduct(@RequestBody List<Integer> productid) {
		double totalSumOfTheProduct = fileService.getTotalSumOfTheProduct(productid);
		return totalSumOfTheProduct;
	}

	@PostMapping("/getTotalAmounts")
	public double getTotalSumOfTheProducts(@RequestBody List<Integer> productid) {
		double totalSumOfTheProduct = fileService.getTotalSumOfTheProducts(productid);
		return totalSumOfTheProduct;
	}

	@GetMapping("/getAllList")
	public ResponseEntity<List<ExcelSheetToDBProductDetails>> getAllTutorials() {
		try {
			List<ExcelSheetToDBProductDetails> tutorials = fileService.getAllTutorials();
			if (tutorials.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<>(tutorials, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/download")
	public ResponseEntity<Resource> getFile() {
		String filename = "tutorials.xlsx";
		InputStreamResource file = new InputStreamResource(fileService.load());
		return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename)
				.contentType(MediaType.parseMediaType("application/vnd.ms-excel")).body(file);
	}

	@PostMapping("gettotalemiestimate/{rate}/{time}/{principal}")
	public HashMap<String, HashMap<Double, Double>> getTotalSumOfTheProducts(@PathVariable double rate,
			@PathVariable double time, @PathVariable double principal) {
		return offlinecustomerdataservice.getInterest(rate, time, principal);
	}


}
