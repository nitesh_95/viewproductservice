package com.bhushan.spring.files.excel.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bhushan.spring.files.excel.model.LoginEntity;
import com.bhushan.spring.files.excel.response.LoginResponse;
import com.bhushan.spring.files.excel.service.LoginService;

@RestController
@CrossOrigin
@RequestMapping("/api/excel")
public class LoginController {

	@Autowired
	private LoginService loginService;

	@PostMapping("/technicianLogin/{username}/{password}")
	public LoginResponse findLogin(@PathVariable Long username, @PathVariable String password) {
		return loginService.getTechnicianLogin(username, password);
	}

	@PostMapping("/adminLogin/{username}/{password}")
	public LoginResponse adminLogin(@PathVariable String username, @PathVariable String password) {
		return loginService.getAdminLogin(username, password);
	}
	@GetMapping("/logins")
	public List<LoginEntity> loginDetails(){
		return loginService.findall();
	}
}
