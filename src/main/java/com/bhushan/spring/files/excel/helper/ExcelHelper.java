package com.bhushan.spring.files.excel.helper;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;

import com.bhushan.spring.files.excel.model.ExcelSheetToDBProductDetails;

public class ExcelHelper {
	public static String TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
	static String[] HEADERs = { "product_id", "product_name", "product_description", "image_url", "product_price", "warrantydays" };
	static String SHEET = "Test";

	public static ByteArrayInputStream tutorialsToExcel(List<ExcelSheetToDBProductDetails> tutorials) {
		try (Workbook workbook = new XSSFWorkbook(); ByteArrayOutputStream out = new ByteArrayOutputStream();) {
			Sheet sheet = workbook.createSheet(SHEET);
			Row headerRow = sheet.createRow(0);
			for (int col = 0; col < HEADERs.length; col++) {
				Cell cell = headerRow.createCell(col);
				cell.setCellValue(HEADERs[col]);
			}
			int rowIdx = 1;
			for (ExcelSheetToDBProductDetails tutorial : tutorials) {
				Row row = sheet.createRow(rowIdx++);
				row.createCell(0).setCellValue(tutorial.getProductid());
				row.createCell(1).setCellValue(tutorial.getProductname());
				row.createCell(2).setCellValue(tutorial.getProductdescription());
				row.createCell(3).setCellValue(tutorial.getImagepath());
				row.createCell(4).setCellValue(tutorial.getPurchaseamount());
				row.createCell(5).setCellValue(tutorial.getWarrantydays());
			}
			workbook.write(out);
			return new ByteArrayInputStream(out.toByteArray());
		} catch (IOException e) {
			throw new RuntimeException("fail to import data to Excel file: " + e.getMessage());
		}
	}

	public static List<ExcelSheetToDBProductDetails> excelToTutorials(InputStream is) {
		try {
			Workbook workbook = new XSSFWorkbook(is);
			Sheet sheet = workbook.getSheet(SHEET);
			Iterator<Row> rows = sheet.iterator();
			List<ExcelSheetToDBProductDetails> tutorials = new ArrayList<ExcelSheetToDBProductDetails>();
			int rowNumber = 0;
			while (rows.hasNext()) {
				Row currentRow = rows.next();
				if (rowNumber == 0) {
					rowNumber++;
					continue;
				}
				Iterator<Cell> cellsInRow = currentRow.iterator();
				ExcelSheetToDBProductDetails tutorial = new ExcelSheetToDBProductDetails();
				int cellIdx = 0;
				while (cellsInRow.hasNext()) {
					Cell currentCell = cellsInRow.next();
					switch (cellIdx) {
					case 0:
						tutorial.setProductid((int) currentCell.getNumericCellValue());
						break;
					case 1:
						tutorial.setProductname(currentCell.getStringCellValue());
						break;
					case 2:
						tutorial.setProductdescription(currentCell.getStringCellValue());
						break;
					case 3:
						tutorial.setImagepath(currentCell.getStringCellValue());
						break;
					case 4:
						tutorial.setPurchaseamount(currentCell.getStringCellValue());
						break;
					case 5:
						tutorial.setWarrantydays(currentCell.getStringCellValue());
						break;
					default:
						break;
					}
					cellIdx++;
				}
				tutorials.add(tutorial);
			}
			workbook.close();
			return tutorials;
		} catch (IOException e) {
			throw new RuntimeException("fail to parse Excel file: " + e.getMessage());
		}
	}
}
