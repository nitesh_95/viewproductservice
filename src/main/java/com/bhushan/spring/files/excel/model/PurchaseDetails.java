package com.bhushan.spring.files.excel.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "purchasedetails")
public class PurchaseDetails {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int purchaseids;
	private String purchasedate;
	private String modeofpayment;
	private boolean emiapplicable;
	private double totalamount;


	public int getPurchaseid() {
		return purchaseids;
	}

	public void setPurchaseid(int purchaseid) {
		this.purchaseids = purchaseid;
	}

	public String getPurchasedate() {
		return purchasedate;
	}

	public void setPurchasedate(String purchasedate) {
		this.purchasedate = purchasedate;
	}

	public String getModeofpayment() {
		return modeofpayment;
	}

	public void setModeofpayment(String modeofpayment) {
		this.modeofpayment = modeofpayment;
	}

	public boolean isEmiapplicable() {
		return emiapplicable;
	}

	public void setEmiapplicable(boolean emiapplicable) {
		this.emiapplicable = emiapplicable;
	}

	public double getTotalamount() {
		return totalamount;
	}

	public void setTotalamount(double totalamount) {
		this.totalamount = totalamount;
	}

}
