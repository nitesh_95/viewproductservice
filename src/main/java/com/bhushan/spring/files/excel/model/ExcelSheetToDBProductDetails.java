package com.bhushan.spring.files.excel.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;


@Entity
@Table(name = "productdetailsdata")
public class ExcelSheetToDBProductDetails {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "productid")
	private int productid;
	@Column(name = "productname")
	private String productname;
	@Column(name = "imagepath")
	private String imagepath;
	@Column(name = "purchaseamount")
	private String purchaseamount;
	@Column(name = "productdescription")
	private String productdescription;
	@Column(name = "warrantydays")
	private String warrantydays;
	@Transient
	private int totalamount;
	@Transient
	private int quantity;
	

	public int getTotalamount() {
		return totalamount;
	}

	public void setTotalamount(int totalamount) {
		this.totalamount = totalamount;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getWarrantydays() {
		return warrantydays;
	}

	public void setWarrantydays(String warrantydays) {
		this.warrantydays = warrantydays;
	}

	public int getProductid() {
		return productid;
	}

	public void setProductid(int productid) {
		this.productid = productid;
	}

	public String getProductname() {
		return productname;
	}

	public void setProductname(String productname) {
		this.productname = productname;
	}

	public String getImagepath() {
		return imagepath;
	}

	public void setImagepath(String imagepath) {
		this.imagepath = imagepath;
	}


	public String getPurchaseamount() {
		return purchaseamount;
	}

	public void setPurchaseamount(String purchaseamount) {
		this.purchaseamount = purchaseamount;
	}

	public String getProductdescription() {
		return productdescription;
	}

	public void setProductdescription(String productdescription) {
		this.productdescription = productdescription;
	}

	@Override
	public String toString() {
		return "AddingProductForView [productid=" + productid + ", productname=" + productname + ", imagepath="
				+ imagepath + ", purchaseamount=" + purchaseamount + ", productdescription=" + productdescription
				+ ", warrantydays=" + warrantydays + ", totalamount=" + totalamount + ", quantity=" + quantity + "]";
	}
}
