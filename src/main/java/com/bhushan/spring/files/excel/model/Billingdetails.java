package com.bhushan.spring.files.excel.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "billingdetails")
public class Billingdetails {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int billingid;
	private double fullamount;
	private double gstrate;
	private double otherTaxRate;
	private double entirerate;
	private double othercharges;
	private double paidamount;
	private double remainingamount;
	private boolean emiapplicable;
	private boolean emicleared;
	@ManyToOne(cascade = { CascadeType.ALL })
	private Emidata emidata;

	public boolean isEmicleared() {
		return emicleared;
	}

	public void setEmicleared(boolean emicleared) {
		this.emicleared = emicleared;
	}

	public Emidata getEmidata() {
		return emidata;
	}

	public void setEmidata(Emidata emidata) {
		this.emidata = emidata;
	}

	public double getRemainingamount() {
		return remainingamount;
	}

	public void setRemainingamount(double remainingamount) {
		this.remainingamount = remainingamount;
	}

	public double getPaidamount() {
		return paidamount;
	}

	public void setPaidamount(double paidamount) {
		this.paidamount = paidamount;
	}

	public boolean isEmiapplicable() {
		return emiapplicable;
	}

	public void setEmiapplicable(boolean emiapplicable) {
		this.emiapplicable = emiapplicable;
	}

	public double getOthercharges() {
		return othercharges;
	}

	public void setOthercharges(double othercharges) {
		this.othercharges = othercharges;
	}

	public double getEntirerate() {
		return entirerate;
	}

	public void setEntirerate(double entirerate) {
		this.entirerate = entirerate;
	}

	public int getBillingid() {
		return billingid;
	}

	public void setBillingid(int billingid) {
		this.billingid = billingid;
	}

	public double getFullamount() {
		return fullamount;
	}

	public void setFullamount(double fullamount) {
		this.fullamount = fullamount;
	}

	public double getGstrate() {
		return gstrate;
	}

	public void setGstrate(double gstrate) {
		this.gstrate = gstrate;
	}

	public double getOtherTaxRate() {
		return otherTaxRate;
	}

	public void setOtherTaxRate(double otherTaxRate) {
		this.otherTaxRate = otherTaxRate;
	}

}
