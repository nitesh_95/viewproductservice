package com.bhushan.spring.files.excel.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "typeofcustomer")
public class TypeOfCustomer {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	private boolean foronlinecustomer;
	private boolean forofflinecustomer;
	private boolean forsparepart;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isForonlinecustomer() {
		return foronlinecustomer;
	}

	public void setForonlinecustomer(boolean foronlinecustomer) {
		this.foronlinecustomer = foronlinecustomer;
	}

	public boolean isForofflinecustomer() {
		return forofflinecustomer;
	}

	public void setForofflinecustomer(boolean forofflinecustomer) {
		this.forofflinecustomer = forofflinecustomer;
	}

	public boolean isForsparepart() {
		return forsparepart;
	}

	public void setForsparepart(boolean forsparepart) {
		this.forsparepart = forsparepart;
	}

}
