package com.bhushan.spring.files.excel.service;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.http.client.ClientProtocolException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bhushan.spring.files.excel.model.Emipayment;
import com.bhushan.spring.files.excel.model.OfflineCustomerDetails;
import com.bhushan.spring.files.excel.repository.OfflineCustomerDetailsRepo;
import com.bhushan.spring.files.excel.response.OfflinCustomerDetailsResponse;

@Service
public class OfflineCustomerDataService {

	@Autowired
	private OfflineCustomerDetailsRepo offlineCustomerDetailsRepo;
	@Autowired
	private MessageUtil messageUtil;

	public OfflinCustomerDetailsResponse saveOfflineCustomerDetails(OfflineCustomerDetails response)
			throws ClientProtocolException, IOException {
		String generatedString = RandomStringUtils.randomNumeric(6);
		String timeStamp = new SimpleDateFormat("dd-MM-yyyy").format(Calendar.getInstance().getTime());
		OfflinCustomerDetailsResponse offlinCustomerDetailsResponse = new OfflinCustomerDetailsResponse();
		response.setName(response.getName());
		response.setMobileno(response.getMobileno());
		response.setInvoiceno(generatedString);
		response.setMailid(response.getMailid());
		response.setDateofpurchase(timeStamp);
		response.setAddress(response.getAddress());
		JSONObject createJson = new JSONObject(response);
		org.json.JSONArray configArray = createJson.getJSONArray("productPurchaseds");
		double sumData = 0;
		ArrayList<Double> arrayList2 = new ArrayList<>();
		for (int i = 0; i < response.getProductPurchaseds().size(); i++) {
			response.getProductPurchaseds().get(i).setCount(i + 1);
			response.getProductPurchaseds().get(i).setTypeofcustomer("OFFLINE_CUSTOMER");
			response.getProductPurchaseds().get(i).setDateofpurchase(timeStamp);
			response.getProductPurchaseds().get(i).getProductnames().setDateofpurchase(timeStamp);
			response.getProductPurchaseds().get(i).getProductnames()
					.setQuantity(response.getProductPurchaseds().get(i).getProductnames().getQuantity());
			double quantity = response.getProductPurchaseds().get(i).getQuantities();
			double purchaseamount = response.getProductPurchaseds().get(i).getUnitprices();
			response.getProductPurchaseds().get(i).getProductnames().setTotalamount(quantity * purchaseamount);
			response.getProductPurchaseds().get(i).getProductnames()
					.setUnitprice(response.getProductPurchaseds().get(i).getProductnames().getPurchaseamount());
			response.getProductPurchaseds().get(i).getProductnames()
					.setDescription(response.getProductPurchaseds().get(i).getProductnames().getWarrantydays());
			response.getProductPurchaseds().get(i).getProductnames()
					.setImagepath(response.getProductPurchaseds().get(i).getProductnames().getImagepath());
			response.getProductPurchaseds().get(i).getProductnames()
					.setProductid(response.getProductPurchaseds().get(i).getProductnames().getProductid());
			response.getProductPurchaseds().get(i).getProductnames()
					.setProductname(response.getProductPurchaseds().get(i).getProductnames().getProductname());
			response.getProductPurchaseds().get(i).getProductnames()
					.setPurchaseamount(response.getProductPurchaseds().get(i).getProductnames().getPurchaseamount());
			int value = (int) quantity;
			response.getProductPurchaseds().get(i).getProductnames().setQuantity(value);
			arrayList2.add(response.getProductPurchaseds().get(i).getProductnames().getTotalamount());
			response.getProductPurchaseds().get(i).getProductnames()
					.setTotalamount(response.getProductPurchaseds().get(i).getProductnames().getTotalamount());
			response.getProductPurchaseds().get(i).getProductnames().setUnitprice(purchaseamount);
			response.getProductPurchaseds().get(i).getProductnames()
					.setWarrantydays(response.getProductPurchaseds().get(i).getProductnames().getWarrantydays());
		}
		double sum = 0;
		for (Double integer : arrayList2) {
			sum += integer;
		}
		response.getBillingdetails().setFullamount(sum);
		double totaltaxrate = response.getBillingdetails().getGstrate()
				+ response.getBillingdetails().getOtherTaxRate();
		double amount = response.getBillingdetails().getFullamount() * totaltaxrate / 100;
		double totalamount = amount + response.getBillingdetails().getFullamount();
		response.getBillingdetails().setGstrate(response.getBillingdetails().getGstrate());
		response.getBillingdetails().setOtherTaxRate(response.getBillingdetails().getOtherTaxRate());
		response.getBillingdetails().setOthercharges(response.getBillingdetails().getOthercharges());
		response.getBillingdetails().setEntirerate(totalamount + response.getBillingdetails().getOthercharges());
		response.getBillingdetails().setPaidamount(response.getBillingdetails().getPaidamount());
		response.getBillingdetails().setRemainingamount(
				response.getBillingdetails().getEntirerate() - response.getBillingdetails().getPaidamount());
		response.getBillingdetails().setEmiapplicable(response.getBillingdetails().isEmiapplicable());
		double sumDatas = 0;
		try {
			double remainingamount = response.getBillingdetails().getRemainingamount();
			double otherTaxRate = response.getBillingdetails().getOtherTaxRate();
			int noofmonths = response.getBillingdetails().getEmidata().getNoofmonths();
			double calculateCompoundInt = calculateCompoundInt(remainingamount, noofmonths, otherTaxRate, 1);
			response.getBillingdetails().getEmidata().setMontlyinstallment(
					calculateCompoundInt / response.getBillingdetails().getEmidata().getNoofmonths());
			response.getBillingdetails().getEmidata()
					.setNoofmonths((response.getBillingdetails().getEmidata().getNoofmonths()));
			response.getBillingdetails().setEmicleared(response.getBillingdetails().getEmidata().getEmiDatas()
					.get(response.getBillingdetails().getEmidata().getEmiDatas().size() - 1).isEmipaymentEnded());

			List<Emipayment> emiDatas = response.getBillingdetails().getEmidata().getEmiDatas();
			ArrayList<Double> arrayList3 = new ArrayList<>();
			for (int i = 0; i < emiDatas.size(); i++) {
				double paidamount = response.getBillingdetails().getEmidata().getEmiDatas().get(i).getPaidamount();
				arrayList3.add(paidamount);
			}
			for (Double integer : arrayList3) {
				sumDatas += integer;
			}
			response.getBillingdetails().getEmidata().getEmiDatas().get(0).setAmount(
					response.getBillingdetails().getEntirerate() - response.getBillingdetails().getPaidamount());
			response.getBillingdetails().getEmidata().getEmiDatas().get(0)
					.setDate(response.getBillingdetails().getEmidata().getEmiDatas().get(0).getDate());
			response.getBillingdetails().getEmidata().getEmiDatas().get(0)
					.setRemainingbalance(response.getBillingdetails().getEntirerate()
							- response.getBillingdetails().getPaidamount() - arrayList3.get(0));
			for (int i = 1; i <= response.getBillingdetails().getEmidata().getEmiDatas().size() - 1; i++) {
				response.getBillingdetails().getEmidata().getEmiDatas().get(i).setAmount(
						response.getBillingdetails().getEmidata().getEmiDatas().get(i - 1).getRemainingbalance());
				response.getBillingdetails().getEmidata().getEmiDatas().get(i).setRemainingbalance(
						response.getBillingdetails().getEmidata().getEmiDatas().get(i - 1).getRemainingbalance()
								- arrayList3.get(i));
				if (response.getBillingdetails().getEmidata().getEmiDatas().get(i).getRemainingbalance() == 0) {
					response.getBillingdetails().getEmidata().getEmiDatas().get(i).setEmipaymentEnded(true);
				} else {
					response.getBillingdetails().getEmidata().getEmiDatas().get(i).setEmipaymentEnded(false);
				}
				if (response.getBillingdetails().getEmidata().getEmiDatas().get(i).getDate() == null) {
					response.getBillingdetails().getEmidata().getEmiDatas().get(i)
							.setDate(response.getDateofpurchase());
				} else {
					response.getBillingdetails().getEmidata().getEmiDatas().get(i)
							.setDate(response.getBillingdetails().getEmidata().getEmiDatas().get(i).getDate());
				}
			}
		} catch (Exception e) {
			response.getBillingdetails().getEmidata().setEmiDatas(null);
		}

		boolean emipaymentEnded = response.getBillingdetails().getEmidata().getEmiDatas()
				.get(response.getBillingdetails().getEmidata().getEmiDatas().size() - 1).isEmipaymentEnded();
		System.out.println("EMI PAYMENT ENDED" + emipaymentEnded);
		response.getBillingdetails().setEmicleared(emipaymentEnded);
		response.getBillingdetails().getEmidata()
				.setRemainingamount(response.getBillingdetails().getRemainingamount() - sumDatas);
		response.getBillingdetails().getEmidata().setEmiDatas(response.getBillingdetails().getEmidata().getEmiDatas());
		OfflineCustomerDetails save = offlineCustomerDetailsRepo.save(response);
		messageUtil.sendInfoForOnlineCustomer(save);
		offlinCustomerDetailsResponse.setOfflinecustomerid(save.getOfflinecustomerid());
		offlinCustomerDetailsResponse.setName(save.getName());
		offlinCustomerDetailsResponse.setMobileno(save.getMobileno());
		offlinCustomerDetailsResponse.setMailid(save.getMailid());
		offlinCustomerDetailsResponse.setInvoiceno(generatedString);
		offlinCustomerDetailsResponse.setBillingdetails(save.getBillingdetails());
		offlinCustomerDetailsResponse.setProductPurchaseds(save.getProductPurchaseds());
		offlinCustomerDetailsResponse.setBillingdetails(save.getBillingdetails());
		offlinCustomerDetailsResponse.setDateofpurchase(save.getDateofpurchase());
		return offlinCustomerDetailsResponse;
	}

	public OfflinCustomerDetailsResponse updateData(OfflineCustomerDetails response)
			throws ClientProtocolException, IOException {
		String generatedString = RandomStringUtils.randomNumeric(6);
		String timeStamp = new SimpleDateFormat("dd-MM-yyyy").format(Calendar.getInstance().getTime());
		OfflinCustomerDetailsResponse offlinCustomerDetailsResponse = new OfflinCustomerDetailsResponse();
		response.setName(response.getName());
		response.setMobileno(response.getMobileno());
		response.setInvoiceno(generatedString);
		response.setMailid(response.getMailid());
		response.setDateofpurchase(timeStamp);
		response.setAddress(response.getAddress());
		HashMap<String, HashMap<Double, Double>> interest = getInterest(response.getBillingdetails().getOtherTaxRate(),
				response.getBillingdetails().getEmidata().getNoofmonths(),
				response.getBillingdetails().getRemainingamount());
		response.setGetdetails(interest);
		ArrayList<Double> arrayList2 = new ArrayList<>();
		for (int i = 0; i < response.getProductPurchaseds().size(); i++) {
			response.getProductPurchaseds().get(i)
					.setProductnamesdata(response.getProductPurchaseds().get(i).getProductnames().getProductname());
			response.getProductPurchaseds().get(i).setCount(i);
			response.getProductPurchaseds().get(i).getProductnames()
					.setQuantity(response.getProductPurchaseds().get(i).getProductnames().getQuantity());
			response.getProductPurchaseds().get(i).setTypeofcustomer("OFFLINE_CUSTOMER");
			response.getProductPurchaseds().get(i).setDateofpurchase(timeStamp);
			response.getProductPurchaseds().get(i).getProductnames().setDateofpurchase(timeStamp);
			response.getProductPurchaseds().get(i).getProductnames().setTypeofcustomer("OFFLINE_CUSTOMER");
			double quantity = response.getProductPurchaseds().get(i).getQuantities();
			double purchaseamount = response.getProductPurchaseds().get(i).getUnitprices();
			response.getProductPurchaseds().get(i).getProductnames().setTotalamount(quantity * purchaseamount);
			response.getProductPurchaseds().get(i).getProductnames()
					.setUnitprice(response.getProductPurchaseds().get(i).getProductnames().getPurchaseamount());
			response.getProductPurchaseds().get(i).getProductnames()
					.setDescription(response.getProductPurchaseds().get(i).getProductnames().getWarrantydays());
			response.getProductPurchaseds().get(i).getProductnames()
					.setImagepath(response.getProductPurchaseds().get(i).getProductnames().getImagepath());
			response.getProductPurchaseds().get(i).getProductnames()
					.setProductid(response.getProductPurchaseds().get(i).getProductnames().getProductid());
			response.getProductPurchaseds().get(i).getProductnames()
					.setProductname(response.getProductPurchaseds().get(i).getProductnames().getProductname());
			response.getProductPurchaseds().get(i).getProductnames()
					.setPurchaseamount(response.getProductPurchaseds().get(i).getProductnames().getPurchaseamount());
			int value = (int) quantity;
			response.getProductPurchaseds().get(i).getProductnames().setQuantity(value);
			arrayList2.add(response.getProductPurchaseds().get(i).getProductnames().getTotalamount());
			response.getProductPurchaseds().get(i).getProductnames()
					.setTotalamount(response.getProductPurchaseds().get(i).getProductnames().getTotalamount());
			response.getProductPurchaseds().get(i).getProductnames().setUnitprice(purchaseamount);
			response.getProductPurchaseds().get(i).getProductnames()
					.setWarrantydays(response.getProductPurchaseds().get(i).getProductnames().getWarrantydays());
		}
		double sum = 0;
		for (Double integer : arrayList2) {
			sum += integer;
		}
		response.getBillingdetails().setFullamount(sum);
		double totaltaxrate = response.getBillingdetails().getGstrate()
				+ response.getBillingdetails().getOtherTaxRate();
		double amount = response.getBillingdetails().getFullamount() * totaltaxrate / 100;
		double totalamount = amount + response.getBillingdetails().getFullamount();
		response.getBillingdetails().setGstrate(response.getBillingdetails().getGstrate());
		response.getBillingdetails().setOtherTaxRate(response.getBillingdetails().getOtherTaxRate());
		response.getBillingdetails().setOthercharges(response.getBillingdetails().getOthercharges());
		response.getBillingdetails().setEntirerate(totalamount + response.getBillingdetails().getOthercharges());
		response.getBillingdetails().setPaidamount(response.getBillingdetails().getPaidamount());
		response.getBillingdetails().setRemainingamount(
				response.getBillingdetails().getEntirerate() - response.getBillingdetails().getPaidamount());
		double remainingamount = response.getBillingdetails().getRemainingamount();
		double otherTaxRate = response.getBillingdetails().getOtherTaxRate();
		int noofmonths = response.getBillingdetails().getEmidata().getNoofmonths();
		double calculateCompoundInt = calculateCompoundInt(remainingamount, noofmonths, otherTaxRate, 4);
		response.getBillingdetails().getEmidata().setMontlyinstallment(calculateCompoundInt);
		response.getBillingdetails().setEmiapplicable(response.getBillingdetails().isEmiapplicable());
		double sumDatas = 0;
		ArrayList<Double> arrayList3 = new ArrayList<>();
		for (Double integer : arrayList3) {
			sumDatas += integer;
		}

		OfflineCustomerDetails save = offlineCustomerDetailsRepo.save(response);
		messageUtil.sendInfoForOnlineCustomer(save);
		offlinCustomerDetailsResponse.setOfflinecustomerid(save.getOfflinecustomerid());
		offlinCustomerDetailsResponse.setName(save.getName());
		offlinCustomerDetailsResponse.setMobileno(save.getMobileno());
		offlinCustomerDetailsResponse.setMailid(save.getMailid());
		offlinCustomerDetailsResponse.setInvoiceno(generatedString);
		offlinCustomerDetailsResponse.setBillingdetails(save.getBillingdetails());
		offlinCustomerDetailsResponse.setProductPurchaseds(save.getProductPurchaseds());
		offlinCustomerDetailsResponse.setBillingdetails(save.getBillingdetails());
		offlinCustomerDetailsResponse.setDateofpurchase(save.getDateofpurchase());
		return offlinCustomerDetailsResponse;
	}

	public static double getdetails(double rate, double time, double principal) {
		double multiplier = Math.pow(1.0 + rate / 100.0, time) - 1.0;
		return multiplier * principal;
	}

	public HashMap<String, HashMap<Double, Double>> getInterest(double rate, double time, double principal) {
		int months[] = { 3, 6, 9, 12 };
		HashMap<Double, Double> hashMap = new HashMap<>();
		HashMap<String, HashMap<Double, Double>> response = new HashMap<>();
		for (double mon : months) {
			double value = principal + getdetails(rate, mon, principal);
			hashMap.put(mon, value);
			response.put("Months and their Interest Rate", hashMap);
		}
		return response;
	}

	public double calculateCompoundInt(double p, int t, double r, double n) {
		double amount = p * Math.pow(1 + (r / n), n * t);
		double cinterest = amount - p;
		return p + cinterest;
	}
}
