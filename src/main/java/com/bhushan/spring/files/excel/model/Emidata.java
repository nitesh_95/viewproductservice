package com.bhushan.spring.files.excel.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "emipayment")
public class Emidata {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int emidataid;
	private double remainingamount;
	private int noofmonths;
	private double montlyinstallment;
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "emidataid")
	private List<Emipayment> emiDatas;

	public double getRemainingamount() {
		return remainingamount;
	}

	public void setRemainingamount(double remainingamount) {
		this.remainingamount = remainingamount;
	}

	public int getNoofmonths() {
		return noofmonths;
	}

	public void setNoofmonths(int noofmonths) {
		this.noofmonths = noofmonths;
	}

	public double getMontlyinstallment() {
		return montlyinstallment;
	}

	public void setMontlyinstallment(double montlyinstallment) {
		this.montlyinstallment = montlyinstallment;
	}

	public int getEmidataid() {
		return emidataid;
	}

	public void setEmidataid(int emidataid) {
		this.emidataid = emidataid;
	}

	public List<Emipayment> getEmiDatas() {
		return emiDatas;
	}

	public void setEmiDatas(List<Emipayment> emiDatas) {
		this.emiDatas = emiDatas;
	}


}
