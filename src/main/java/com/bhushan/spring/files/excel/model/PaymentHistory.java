package com.bhushan.spring.files.excel.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "paymentdetails")
public class PaymentHistory {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	private String paymentstatus;
	private String dateofdelivery;
	private String orderstatus;
	private String paymentdate;
	private long transactionid;
	private double deliverycharges;
	private double totalcostAfterapplyingdeliverycharges;
	
	
	public double getTotalcostAfterapplyingdeliverycharges() {
		return totalcostAfterapplyingdeliverycharges;
	}
	public void setTotalcostAfterapplyingdeliverycharges(double totalcostAfterapplyingdeliverycharges) {
		this.totalcostAfterapplyingdeliverycharges = totalcostAfterapplyingdeliverycharges;
	}
	public double getDeliverycharges() {
		return deliverycharges;
	}
	public void setDeliverycharges(double deliverycharges) {
		this.deliverycharges = deliverycharges;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDateofdelivery() {
		return dateofdelivery;
	}
	public void setDateofdelivery(String dateofdelivery) {
		this.dateofdelivery = dateofdelivery;
	}
	public String getOrderstatus() {
		return orderstatus;
	}
	public void setOrderstatus(String orderstatus) {
		this.orderstatus = orderstatus;
	}
	public String getPaymentdate() {
		return paymentdate;
	}
	public void setPaymentdate(String paymentdate) {
		this.paymentdate = paymentdate;
	}
	public long getTransactionid() {
		return transactionid;
	}
	public void setTransactionid(long transactionid) {
		this.transactionid = transactionid;
	}
	public String getPaymentstatus() {
		return paymentstatus;
	}
	public void setPaymentstatus(String paymentstatus) {
		this.paymentstatus = paymentstatus;
	}
	
	
	
}
