package com.bhushan.spring.files.excel.response;

import com.bhushan.spring.files.excel.model.OfflineCustomerDetails;

public class OfflineCustomerResponse {

	private String status;
	private String message;
	private OfflineCustomerDetails offlineCustomerDetails;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public OfflineCustomerDetails getOfflineCustomerDetails() {
		return offlineCustomerDetails;
	}

	public void setOfflineCustomerDetails(OfflineCustomerDetails offlineCustomerDetails) {
		this.offlineCustomerDetails = offlineCustomerDetails;
	}

}
