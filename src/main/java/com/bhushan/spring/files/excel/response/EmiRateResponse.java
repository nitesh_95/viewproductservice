package com.bhushan.spring.files.excel.response;

import java.util.HashMap;

public class EmiRateResponse {
	
	private HashMap<String, HashMap<Double, Double>> getdetails;

	public HashMap<String, HashMap<Double, Double>> getGetdetails() {
		return getdetails;
	}

	public void setGetdetails(HashMap<String, HashMap<Double, Double>> getdetails) {
		this.getdetails = getdetails;
	}
 
}
