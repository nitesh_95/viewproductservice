package com.bhushan.spring.files.excel.response;

import java.util.List;

import com.bhushan.spring.files.excel.model.Billingdetails;
import com.bhushan.spring.files.excel.model.ProductPurchased;

public class OfflinCustomerDetailsResponse {

	private int offlinecustomerid;
	private String name;
	private String mailid;
	private long mobileno;
	private String invoiceno;
	private String dateofpurchase;
	private List<ProductPurchased> productPurchaseds;
	private Billingdetails billingdetails;

	public String getDateofpurchase() {
		return dateofpurchase;
	}

	public void setDateofpurchase(String dateofpurchase) {
		this.dateofpurchase = dateofpurchase;
	}

	public String getName() {
		return name;
	}

	public int getOfflinecustomerid() {
		return offlinecustomerid;
	}

	public void setOfflinecustomerid(int offlinecustomerid) {
		this.offlinecustomerid = offlinecustomerid;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMailid() {
		return mailid;
	}

	public void setMailid(String mailid) {
		this.mailid = mailid;
	}

	public long getMobileno() {
		return mobileno;
	}

	public void setMobileno(long mobileno) {
		this.mobileno = mobileno;
	}

	public String getInvoiceno() {
		return invoiceno;
	}

	public void setInvoiceno(String invoiceno) {
		this.invoiceno = invoiceno;
	}

	public List<ProductPurchased> getProductPurchaseds() {
		return productPurchaseds;
	}

	public void setProductPurchaseds(List<ProductPurchased> productPurchaseds) {
		this.productPurchaseds = productPurchaseds;
	}

	public Billingdetails getBillingdetails() {
		return billingdetails;
	}

	public void setBillingdetails(Billingdetails billingdetails) {
		this.billingdetails = billingdetails;
	}

}
